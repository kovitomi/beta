package proto;

/**
 * Szinek. Ez egy enum (felsorolás), a színekért (kék, sárga (narancs), zöld, piros) és kezelésükért felelős.
 *
 */
public enum Color {
	Blue, Orange, Red, Green;
	
	/**
	 * Azt a színt adja vissza, amelyik a párja (a féregjáratok (Portal) szerint: kék-sárga (narancs), zöld-piros) annak a színnek, amelyiken meghívjuk.
	 * @return A szin.
	 */
	public Color getOther() {
		if(this == Blue) return Orange;
		if(this == Orange) return Blue;
		if(this == Red) return Green;
		else return Red;
	}
	
}
