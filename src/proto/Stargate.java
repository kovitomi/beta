package proto;

/** 
 * Csillagkapu. Egy csillagkaput reprezentál, tudja mi a színe, merre néz, ismeri a Portalt.
 *
 */
public class Stargate extends Side {
	
	/** 
	 * A színe.
	 */
	private Color color = null;

	/** 
	 * A féregjárat (Portal).
	 */
	private Portal portal = null;
	
	/** 
	 * Az irány, amerre néz.
	 */
	private Direction viewDirection = null;
	
	/**
	 * Csillagkapu konstruktor. Színt, Portal-t beállítja, viewDirection null.
	 * @param newColor Csillagkapu színe.
	 * @param newPortal A féregjárat.
	 */
	public Stargate(Color color, Portal portal) {
		super("");
		this.color = color;
		this.portal = portal;
		this.viewDirection = null;
	}
	
	/**
	 * Visszaadja a színét.
	 * @return A szín.
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Visszaadja az irányt, amerre néz.
	 * @return Az irány.
	 */
	public Direction getDirection() {
		return viewDirection;
	}
	
	/**
	 * Beállítja nézési irányt.
	 * @param viewDirection Az irány.
	 */
	public void setDirection(Direction viewDirection) {
		this.viewDirection = viewDirection;
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: a Portal-tól kérdezzük meg, hogy átlehet-e menni a féregjáraton (nyitott-e), ha igen lekérjük a Portal-tól a másik színű csillagkapu
	 * (ami a párja) nézési irányát (getDirection(), lásd Portal), majd ebbe az irányba forgatjuk a mozgatható objektumot (look(), lásd Movable).
	 * @param movable A mozgatható objektum.
	 * @return Igen/Nem.
	 */
	@Override
	public boolean canMove(Movable movable) {
		if(portal.isPermeable(color)) {
			if(movable != null) {
				movable.look(portal.getDirection(color.getOther()));
			}
			return true;
		} else return false;
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: a Portal-tól elkérjük a másik szín (a párja ennek) mezőjét (getTile(), lásd Portal).
	 * @param direction Az irány.
	 * @return A mező.
	 */
	@Override
	public Tile getTile(Direction direction) {
		return portal.getTile(color.getOther());
	}
	
}
