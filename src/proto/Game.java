package proto;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Stack;




/** 
 * Jatek. A játék kezeléséért, felhasználóval kapcsolat tartásért, inicializálásáért felelős.
 *
 */
public class Game {
	
	/**
	 * Az ezredesre referencia.
	 */
	private Character oneill = null;
	
	/**
	 * A jaffára referencia.
	 */
	private Character jaffa = null;
	
	/**
	 * Az éppen élő replikátorra referencia.
	 */
	private Replicator replicator = null;
	
	/**
	 * Referencia a pályára.
	 */
	private Map map = null;
	
	/**
	 * Teszt-e.
	 */
	private Boolean isTest = false;
	/**
	 * Kezeli a menüt: ő fogadja és dolgozza fel a parancsokat. Játék indításakor meghívja az initialize()-t.
	 * @param args
	 */
	
	/**
	 * Tároljuk, hogy a játék véget ért-e, illetve hogy ki nyerte.
	 */
	private Boolean ended = null;
	
	public static void main(String[] args) {	
		Game game = new Game();
        game.inputHandling();
	}
	
	/**
	 * Lekezeli a bementi parancsokat, és minden esetben elindítja a megfelelő mechanizmust.
	 * @param game A játék objektum.
	 */
	public void inputHandling(){
		String command = "";
        Scanner input = new Scanner( System.in );
        boolean scanning = true;
        int zpm = 0;
        
        while( scanning ) {
        	//System.out.println("Bemeneti parancs: ");
        	command = input.next();
        	switch(command){
        	case "Jatek":
        		command = input.next();
        		switch(command){
        		case "elkezd":
        			URL url = null;
        			File file = null;
            		command = input.next();
            		switch(command){
            		case "rendes"://
            			isTest = false;
            			command = input.next();
            			url = getClass().getResource("maps/"+ command + ".txt");
            			file = new File(url.getPath());
                		this.initialize( file );
            			break;
            		case "teszt"://
            			isTest = true;
            			url = getClass().getResource("maps/map01.txt");
            			file = new File(url.getPath());
                		this.initialize( file );
            			break;
            		default:
            			System.out.println("A megadott parancs helytelen.");	
            		}
        			break;
        		case "kilep":
            		scanning = false;
        			break;
            	default:
            		System.out.println("A megadott parancs helytelen.");
            		break;
        		}
        		break;
        	case "Oneill":
        		command = input.next();
        		switch(command){
        		case "menj":
        			command = input.next();
        			switch(command){
        			case "eszakra":
        				oneill.move(Direction.North);
        				break;
        			case "nyugatra":
        				oneill.move(Direction.West);
        				break;
        			case "delre":
        				oneill.move(Direction.South);
        				break;
        			case "keletre":
        				oneill.move(Direction.East);
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
        			break;
	        	case "lojj":
	        		command = input.next();
        			switch(command){
        			case "keket":
        				oneill.shootStargate(Color.Blue);
        				break;
        			case "sargat":
        				oneill.shootStargate(Color.Orange);
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
	        		break;
        		case "veddfel":
        			oneill.pickUpBox();
            		break;
        		case "teddle":
        			oneill.putDownBox();
            		break;
            	case "nezz":
        			command = input.next();
        			switch(command){
        			case "balra":
        				oneill.look(oneill.viewDirection.getLeft());
        				break;
        			case "jobbra":
        				oneill.look(oneill.viewDirection.getRight());
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
            		break;
            	case "taska":
            		oneill.showStateMovable();
            		break;
    			case "maxzpm":
    				zpm = input.nextInt();
    				oneill.setMaxZpm(zpm);
    				break;
            	default:
            		System.out.println("A megadott parancs helytelen.");
            		break;
        		}
        		break;
        	case "Jaffa":
        		command = input.next();
        		switch(command){
        		case "menj":
        			command = input.next();
        			switch(command){
        			case "eszakra":
        				jaffa.move(Direction.North);
        				break;
        			case "nyugatra":
        				jaffa.move(Direction.West);
        				break;
        			case "delre":
        				jaffa.move(Direction.South);
        				break;
        			case "keletre":
        				jaffa.move(Direction.East);
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
        			break;
	        	case "lojj":
	        		command = input.next();
        			switch(command){
        			case "zoldet":
        				jaffa.shootStargate(Color.Green);
        				break;
        			case "pirosat":
        				jaffa.shootStargate(Color.Red);
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
	        		break;
        		case "veddfel":
        			jaffa.pickUpBox();
            		break;
        		case "teddle":
        			jaffa.putDownBox();
            		break;
            	case "nezz":
        			command = input.next();
        			switch(command){
        			case "balra":
        				jaffa.look(jaffa.viewDirection.getLeft());
        				break;
        			case "jobbra":
        				jaffa.look(jaffa.viewDirection.getRight());
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
            		break;
            	case "taska":
            		jaffa.showStateMovable();
            		break;
    			case "maxzpm":
    				zpm = input.nextInt();
    				jaffa.setMaxZpm(zpm);
    				break;
            	default:
            		System.out.println("A megadott parancs helytelen.");
            		break;
        		}
        		break; 
        	case "Replikator":
        		command = input.next();
        		switch(command){
        		case "menj":
        			command = input.next();
        			switch(command){
        			case "eszakra":
        				replicator.move(Direction.North);
        				break;
        			case "nyugatra":
        				replicator.move(Direction.West);
        				break;
        			case "delre":
        				replicator.move(Direction.South);
        				break;
        			case "keletre":
        				replicator.move(Direction.East);
        				break;
                	default:
                		System.out.println("A megadott parancs helytelen.");
                		break;
        			}
        			break;
            	default:
            		System.out.println("A megadott parancs helytelen.");
            		break;
        		}
        		break;
	        case "Vilag":
	        	command = input.next();
	        	switch(command){
	        	case "allapot":
	        		map.showStateAll();
	        		break;
	        	default:
	        		System.out.println("A megadott parancs helytelen.");
	        		break;
	        	}
	    		break;
        	default:
        		System.out.println("A megadott parancs helytelen.");
        		break;
        	}
        }      
        
		input.close();
	}
	
	/**
	 * Létrehozza a Portalt, majd a mezőket (Tile), oldalakat (Side), karaktereket (Char, Character), replikátort (Repl, Replicator) hozzuk létre. Mezőket, oldalakat osztályonként különbőző LinkedHashMap-be rakjuk, az azonosítójukat listába (pl. ArrayList). Beállítjuk a mezőkben az oldalak referenciáit, az oldalakban a mezők referenciáit (Conn). Létrehozzuk a mezőket (Bal, Balance) LinkedHashMap-ben, beállítjuk az ajtókra (Door) a referenciákat, az ajtókban pedig a mérlegekre. A pályán elhelyezzük az ezredest, a jaffát és a replikátort. Létrehozzuk a Map-et és átadjuk neki az objektumokat.
	 * @param map A térkép fájl.
	 */
	public void initialize(File map) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(map);
		} catch (FileNotFoundException e) {
		}
		String line = "";
		boolean scanning = true;
		String[] temp1;
		String[] temp2;
		LinkedHashMap<String, Tile> tiles = new LinkedHashMap<String, Tile>();
		LinkedHashMap<String, Side> sides = new LinkedHashMap<String, Side>();
		LinkedHashMap<String, Balance> balances = new LinkedHashMap<String, Balance>();
		LinkedHashMap<String, String> balancePlaces = new LinkedHashMap<String, String>();
		
		String oniellPlace = "";
		String jaffaPlace = "";
		String replicatorPlace = "";
		
		while(scanning){
			line = scanner.nextLine();
			temp1 = line.split("-");
			Stack<Integer> box = new Stack<Integer>();
			Direction dir = Direction.North;
			
			if(temp1[0] != null){
				switch(temp1[0]){
				case "Tile":
					temp2 = temp1[1].split(",");
					if(Integer.parseInt(temp2[1]) == 1){
						tiles.put(temp2[0], new Abyss(temp2[0], false, new Stack<Integer>()));
					}
					else{
						box.push(Integer.parseInt(temp2[3]));
						Boolean zpm = false;
						if(Integer.parseInt(temp2[2])==1) zpm = true;
						tiles.put(temp2[0], new Tile(temp2[0], zpm, box));
					}
					break;
				case "Side":
					temp2 = temp1[1].split(",");
					switch(temp2[1]){
					case "wall":
						sides.put(temp2[0], new Wall(temp2[0]));
						break;
					case "open":
						sides.put(temp2[0], new Side(temp2[0]));
						break;
					case "spec":
						sides.put(temp2[0], new SpecialWall(temp2[0]));
						break;
					case "door":
						sides.put(temp2[0], new Door(temp2[0]));
						break;
					}
					break;
				case "Char":
					temp2 = temp1[1].split(",");
					switch(temp2[6]){
					case "North":
						dir = Direction.North;
						break;
					case "South":
						dir = Direction.South;
						break;
					case "West":
						dir = Direction.West;
						break;
					case "East":
						dir = Direction.East;
						break;
					}
					switch(temp2[0]){
					case "Colonel":
						oneill = new Colonel(this,Integer.parseInt(temp2[2]),Integer.parseInt(temp2[3]),Integer.parseInt(temp2[5]),Integer.parseInt(temp2[4]), dir);
						oniellPlace = temp2[1];
						break;
					case "Jaffa":
						jaffa = new Jaffa(this,Integer.parseInt(temp2[2]),Integer.parseInt(temp2[3]),Integer.parseInt(temp2[5]),Integer.parseInt(temp2[4]), dir);
						jaffaPlace = temp2[1];
						break;
					}
					break;
				case "Bal":
					temp2 = temp1[1].split(",");
					balances.put(temp2[0], new Balance(temp2[0], (Door)sides.get(temp2[2]), Integer.parseInt(temp2[1])));
					balancePlaces.put(temp2[3], temp2[0]);
					break;
				case "Conn":
					temp2 = temp1[1].split(",");
					LinkedHashMap<Direction, Side> sidesForTile = new LinkedHashMap<Direction, Side>();
					sidesForTile.put(Direction.North, sides.get(temp2[1]));
					sidesForTile.put(Direction.West, sides.get(temp2[2]));
					sidesForTile.put(Direction.East, sides.get(temp2[3]));
					sidesForTile.put(Direction.South, sides.get(temp2[4]));
					tiles.get(temp2[0]).setSides(sidesForTile, balances.get(balancePlaces.get(temp2[0])));
					sides.get(temp2[1]).setTiles(Direction.South, tiles.get(temp2[0]));
					sides.get(temp2[2]).setTiles(Direction.East, tiles.get(temp2[0]));
					sides.get(temp2[3]).setTiles(Direction.West, tiles.get(temp2[0]));
					sides.get(temp2[4]).setTiles(Direction.North, tiles.get(temp2[0]));					
					break;
				case "Replicator":
					temp2 = temp1[1].split(",");
					switch(temp2[3]){
					case "North":
						dir = Direction.North;
						break;
					case "South":
						dir = Direction.South;
						break;
					case "West":
						dir = Direction.West;
						break;
					case "East":
						dir = Direction.East;
						break;
					}
					replicator = new Replicator(this, dir);
					replicatorPlace = temp2[1];
					break;
				default:
					System.out.println("A megadott típus nem értelmezhető.");
					break;
				}
			}
							
			if(!scanner.hasNextLine()){
				scanning = false;
				oneill.setLocation(tiles.get(oniellPlace));
				tiles.get(oniellPlace).setOneill(true);
				tiles.get(oniellPlace).weightChanged(oneill.getWeight());
				jaffa.setLocation(tiles.get(jaffaPlace));
				tiles.get(jaffaPlace).setJaffa(true);
				tiles.get(jaffaPlace).weightChanged(jaffa.getWeight());
				replicator.setLocation(tiles.get(replicatorPlace));
				tiles.get(replicatorPlace).setReplicator(true);
				this.map = new Map((Colonel)oneill, (Jaffa)jaffa, replicator, new Portal(), this, tiles, sides, balances, isTest);
				oneill.setMap(this.map);
				jaffa.setMap(this.map);
				replicator.setMap(this.map);
			}
		}
	}
	
	/**
	 * Ha az ezredes meghal, ezt a függvényt hívja. Kiírja majd az isEnded-et hívva, hogy “Jaffa nyert!”.
	 */
	public void oneillLose() {
		ended = false;
	}
	
	/**
	 * Ha a jaffa meghal, ezt a függvényt hívja. Kiírja majd az isEnded-et hívva, hogy “Ezredes nyert!”.
	 */
	public void jaffaLose() {
		ended = true;
	}
	
	/**
	 * Ha az ezredes begyűjtött annyi ZPM-et, amennyit kell neki ezt a függvényt hívja. Kiírja  majd az isEnded-et hívva, hogy “Ezredes nyert!”.
	 */
	public void oneillVictory() {
		ended = true;
	}
	
	/**
	 * Ha a jaffa begyűjtött annyit ZPM-et, amennyit kell neki ezt a függvéynt hívja. Kiírja majd az isEnded-et hívva, hogy “Jaffa nyert!”.
	 */
	public void jaffaVictory() {
		ended = false;
	}
	
	/**
	 * Ezt kell hívni, ha a replikátor meghal.
	 */
	public void replicatorDied() {
		replicator = null;
	}
	
	/**
	 * Kiírja, ha a játék véget ért.
	 */
	public void isEnded(){
		if(ended != null && ended == false) System.out.println("Jaffa nyert!");
		if(ended != null && ended == true) System.out.println("Ezredes nyert!");
	}
	
}
