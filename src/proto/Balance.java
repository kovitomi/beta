package proto;

/** 
 * Merleg. Egy hozzárendelt ajtó vezérlését végzi (nyitja, zárja) annak függvényében, hogy a mezőre (amin van) helyezett súly átlép-e egy küszöb (v. határ) értéket.
 *
 */
public class Balance {
	
	/**
	 * Azonosító.
	 */
	private String id = "";
	
	/**
	 * A határérték: ha e fölött van a mezőben súly, a hozzárendelt ajtó nyitva van, ha ez alatt csukva.
	 */
	private int weightLimit = 0;
	
	/**
	 * Referencia az ajtóra.
	 */
	private Door door = null;
	
	/**
	 * Beállítja az azonosítót, a  door-t a kapott referenciára, a weightLimit értékét a kapott értékre.
	 * @param id Az azonosító.
	 * @param door A mérleghez tartozó ajtó.
	 * @param weightLimit A mérleghez tartozó súlykorlát.
	 */
	public Balance(String id, Door door, int weightLimit) {
		this.id = id;
		this.door = door;
		this.weightLimit = weightLimit;
	}
	/**
	 * Akkor hívja meg a mező (Tile), amin van, amikor azon megváltozik az összsúly, ezt a súlyt paraméterben átadja. Ha ez a súly nagyobb,
	 * vagy egyenlő a határsúllyal (weightLimit), az ajtó nyitott állapotba kerül, ha kisebb zártba (az ajtó (Door) setOpen() metódusát hívja).
	 * @param weight Az összsúly a mérlegen.
	 */
	public void weightChanged(int weight) {
		if(weight >= weightLimit){
			door.setOpen(true);
		}else{
			door.setOpen(false);
		}
	}
	
	/**
	 * Az ajtót adja vissza, amit vezérel.
	 * @return Az ajto.
	 */
	public Door getDoor() {
		return door;
	}
	
	/**
	 * Visszaadja az azonosítót.
	 * @return Azonosító.
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Visszaadja  súlyhatárt.
	 * @return A súlyhatár.
	 */
	public int getWeightLimit() {
		return weightLimit;
	}
}
