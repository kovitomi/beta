package proto;

/**
 * Karakter. Az ezredest és a jaffát megvalósító osztály, tud mozogni, lőni, dobozt felvenni/arrébb vinni/letenni, ZPM-et gyűjteni, ha szakadékba esik meghal.
 *
 */
public abstract class Character extends Movable{

	/**
	 * A nála levő doboz súlya (ha nincs nála, akkor 0).
	 */
	protected int boxWeight = 0;
	
	/**
	 * A ZPM modulok száma, ami nála van.
	 */
	protected int zpm = 0;
	
	/**
	 * A ZPM modulok száma, amit össze kell gyűjteni a játék megnyeréséig.
	 */
	protected int maxzpm = 0;

	/**
	 * Megpróbál felvenni egy dobozt (ha nincs nála, azaz boxWeight==0) a szemközti mezőről: a mezőn, amin áll lekéri adott irányban (amerre néz, viewDirection) az oldalt (Side), ha ezen áttud menni (Side-on canMove() true-t adi vissza), elkéri a Side-tól az adott irányban (viewDirection) a mezőt, annak meghívja a gatherBox() metódusát, ami ha van a mezőn doboz leveszi a Stack tetejéről, meghívja a karakter giveBox() metódusát, átadja a doboz súlyát paraméterben (lásd: Tile).
	 */
	public void pickUpBox(){
		Side inWay = location.tileSide(viewDirection);
		Tile boxLocation = null;
		if(inWay.canMove(this)) {
			boxLocation = inWay.getTile(viewDirection);
			if(boxWeight == 0) boxLocation.gatherBox(this);
		}
		location.showStateTile();
		if(boxLocation != null) boxLocation.showStateTile();
		showStateMovable();
	}
	
	/**
	 * Ha van nála doboz (boxWeight>0), a fentivel megegyező módon megszerzni a referenciát a szemközti mezőre (ha átlehet oda menni),
	 * meghívja a mező placeBox() metódusát, aminek átadja paraméterben a doboz súlyát, a mező felrakja a Stackre (lásd Tile).
	 */
	public void putDownBox(){
		if(boxWeight == 0) return;
		
		Side inWay = location.tileSide(viewDirection);
		Tile boxLocation = null;
		if(inWay.canMove(this)) {
			boxLocation = inWay.getTile(viewDirection);
			boxLocation.placeBox(this);
		}
		location.showStateTile();
		if(boxLocation != null) boxLocation.showStateTile();
		showStateMovable();
	}
	
	/**
	 * Paraméterben adott színű csillagkaput lő előre (viewDirection), meghívja a Map shoot() metódusát, aminek átadja a tartózkodási mezőjét, az irányt és a színt (lásd Map).
	 * @param color A színe a lövedéknek.
	 */
	public void shootStargate(Color color){
		map.shoot(location, viewDirection, color);
	}
	
	/**
	 * Mező ezt hívja, ha dobozt ad a karakternek (aki tőle előtte kért dobozt). Szól a mezőnek amin áll, hogy mennyivel változott az összsúlya.
	 * @param boxWeight A doboz súlya.
	 */
	public void giveBox(int boxWeight){
		this.boxWeight = boxWeight;
		location.weightChanged(boxWeight);
	}
	
	/**
	 * Mező hívja, ha dobozt vesz el a karaktertől (aki előtte leakarta tenni a dobozt). Szól a mezőnek amin áll, hogy mennyivel változott az összsúlya.
	 * @return A doboz súlya.
	 */
	public int takeBox(){
		int weight = boxWeight;
		this.boxWeight = 0;
		
		//Csökken a súlya.
		location.weightChanged((-1) * weight);
		return weight;
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: a karakter súlyának és a nála levő doboz súlyának az összegét adjuk vissza.
	 * @return A karakter és a nála lévő doboz súlya.
	 */
	@Override
	public int getWeight(){
		return selfWeight + boxWeight;
	}
	
	/**
	 * Beállítja a pályát.
	 * @param map A pálya.
	 */
	public void setMap(Map map){
		this.map = map;
	}
	
	/**
	 * Beállítja az összeszedendő zpm-ek számát.
	 * @param maxzpm Az összegyűjtendő zpm-ek száma.
	 */
	public void setMaxZpm(int maxzpm){
		this.maxzpm = maxzpm;
	}
	
	/**
	 * Felveszi a ZPM-et.
	 */
	public abstract boolean pickUpZpm();
	
	/**
	 * Meghal a karakter.
	 * @param A szakadék, amibe beleesett.
	 */
	public abstract void die(Abyss abyss);
	
}
