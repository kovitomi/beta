package proto;

/** 
 * Speciális fal. Falként viselkedik annyi különbséggel, hogy lehet rá csillagkaput lőni. Ha van rajta csillagkapu, bevonja a mozgás kezelésébe.
 *
 */
public class SpecialWall extends Wall {

	/** 
	 * Referencia a csillagkapura (Stargate), ami rajta van, null ha nincs rajta.
	 */
	private Stargate stargate = null;
	/** 
	 * True, ha van rajta csillagkapu, false, ha nincs.
	 */
	private boolean isStargateOnIt = false;
	
	/**
	 * Meghívja az ős konstruktorát, stargate-t null-ba, isStargateOnIt-t false-ba.
	 */
	public SpecialWall(String id){
		super(id);
		this.stargate = null;
		this.isStargateOnIt = false;
		type = "spec";
	}
	
	/** 
	 * Referenciát kap egy csillagkapura, ezt beállítja és isStargateOnIt-et true-ba állítja.
	 * @param stargate A csillagkapu.
	 */
	public void setStargate(Stargate stargate) {
		this.stargate = stargate;
		if(stargate==null) isStargateOnIt = false;
		else isStargateOnIt = true;
	}
	
	/** 
	 * Visszaadja isStargateOnIt-et (van-e rajta csillagkapu).
	 * @return Van/Nincs.
	 */
	public Boolean isStargate() {
		return isStargateOnIt;
	}
	
	/**
	 * Visszaadja az adott irányban levő mezőt (az örökölt tiles indexelésével).
	 * @param direction Az irány.
	 * @return A mező.
	 */
	public Tile getRealTile(Direction direction) {
		return super.getTile(direction);
	}
	
	/**
	 * Visszaadja a csillagkaput, ami rajta van, null-t, ha nincs rajta.
	 * @return A csillagkapu. null, ha nincs.
	 */
	public Stargate getStargate() {
		return stargate;
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: ha van rajta csillagkapu, továbbítja neki a kérést (canMove(), lásd Stargate), ha nincs false-t ad vissza.
	 * @param movable A mozgatható objektum.
	 * @return Igen/Nem.
	 */
	@Override
	public boolean canMove(Movable movable) {
		if(stargate == null){
			return false;
		}else{
			return stargate.canMove(movable);
		}
	}
	
	/** 
	 * Felüldefiniáljuk az örököltet: ha van rajta csillagkapu, továbbítja annak a kérést (getTile(), lásd Stargate), és az onnan kapottat adja vissza,
	 * ha nincs rajta, akkor nullt.
	 * @param direction Az irány. 
	 * @return A mező.
	 */
	@Override
	public Tile getTile(Direction direction) {
		//Ha van rajta csillagkapu, akkor attól kéri el (féregjárat túl oldalán lévő kell ilyenkor)
		if(stargate != null) return stargate.getTile(direction);
		else return null;
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: true-t ad vissza.
	 * @return Igen.
	 */
	@Override
	public Boolean isStargatePlacable() {
		return true;
	}
	
	/**
	 * Ha van rajta csillagkapu, attól kéri le az <átjárható>, <mező2> és <csillagkapu> értékeket (mező2: a féregjárat túl oldalán levő mező azonosítója, ha a féregjárat átjárható, null, ha nem az).
	 */
	@Override
	public void showStateSide(){
		if(isStargateOnIt) {
			stargateColor = stargate.getColor().toString();
		}
		super.showStateSide();
	}
	
}
