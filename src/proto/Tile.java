package proto;
import java.util.LinkedHashMap;
import java.util.Stack;


/** 
 * Mező. Egy sima mezőt reprezentál (ebből származik a szakadék is), tudja, hogy rajta van-e az ezredes, a jaffa, van-e rajta replikátor, ZPM, Stackszerűen kezeli a rajta levő dobozokat, a rajta levő mérlegnek (ha van rajta) jelzi a súlyváltozásokat, ismeri az odlalait.
 *
 */
public class Tile{

	/**
	 * Azonosító.
	 */
	protected String id = "";
	
	/** 
	 * Az oldalait tárolja az adott irányban.
	 */
	protected LinkedHashMap<Direction, Side> sides = new LinkedHashMap<Direction, Side>();
	
	/**
	 * Stack, amiben a dobozok súlyai az egyes értékek, mindig a legfelső lehet lekérni, tetejére lehet újat rakni.
	 */
	protected Stack<Integer> box = new Stack<Integer>();
	
	/**
	 * True, ha van rajta, false, ha nincs rajta ZPM.
	 */
	protected boolean zpm = false;
	
	/**
	 * True, ha rajta áll az ezredes (Oneill), false, ha nem.
	 */
	protected boolean oneillStay = false;
	
	/**
	 * Ha rajta áll a jaffa true, ha nem false.
	 */
	protected boolean jaffaStay = false;
	
	/**
	 * True, ha van rajta van a replikátor, false, ha nincs.
	 */
	protected boolean replicatorStay = false;
	
	/**
	 * A mérlegre referencia, ha nincs rajta null.
	 */
	private Balance balance = null;

	/**
	 * A rajta tartózkodó karakterek összsúlya.
	 */
	protected int characterWeight = 0;
	
	/**
	 * CSAK a kiíráshoz használható!
	 */
	protected String type = "";
	
	/**
	 * Beállítja, hogy van-e rajta ZPM, beállítja a dobozokat. oneillStay, jaffaStay, replicatorStay false.
	 * @param id A mező azonosítója.
	 * @param zpm Van-e a mezőn zpm.
	 * @param box A doboz stack, ami a mezőn van.
	 */
	public Tile(String id, boolean zpm, Stack<Integer> box){
		this.id = id;
		this.zpm = zpm;
		this.box = box;
		oneillStay = false;
		jaffaStay = false;
		replicatorStay = false;
		type = "tile";
	}
	
	/**
	 * Beállítja az oldalakat (sides) és a mérleget (balance, lehet null).
	 * @param sides A beálltandó oldalak.
	 * @param balance A beállítandó mérleg.
	 */
	public void setSides(LinkedHashMap<Direction, Side> sides, Balance balance) {
		this.sides = sides;
		this.balance = balance;
	}
	
	/**
	 * Az adott irányban levő oldalt adja vissza (sides indexelésével).
	 * @param direction Az irany.
	 * @return Az oldal.
	 */
	public Side tileSide(Direction direction) {
		return sides.get(direction);
	}
	
	/**
	 * Az a mező hívja, akiről elmegy valaki, ezt a valakit átadja paraméterként. Ha van rajta ZPM meghívja a Movable pickUpZpm() metódusát az érkezetten,
	 * ha az igazat ad vissza, zpm-et false-ba rakja. Lekéri az érkezett súlyát, majd ezt hozzáadva a dobozok összsúlyához szól a mérlegnek (weightChanged()),
	 * hogy mennyi az új összsúly, ami rajta van (lásd Balance), majd az érkezettnek beállítja az új tartózkodási helyét (setLocation(), saját magát adja át).
	 * @param movable A mozgatható objektum.
	 */
	public void arrive(Movable movable) {
		characterWeight += movable.getWeight();
		movable.setLocationBool(this, true);
		if(balance != null){
			balance.weightChanged(boxFullWeight() + characterWeight);
		}
		if(zpm) {
			if(movable.pickUpZpm()) {
				zpm = false;
			}
		}
		movable.setLocation(this);
	}
	
	/**
	 * Távozáskor hívják rajta. Ha van rajta mérleg és az a mérleg által nyitott ajtó pont az az oldal, amelyiken átakar menni,
	 * nem mehet át (ehhez kell a Direction: lekérjük abban az irányban az oldalt (sides indexelésével) és az ajtót (getDoor(), lásd Balance)).
	 * Ha nem ugyanaz, akkor a paraméterben kapott mezőn meghívjuk az arrive()-ot a kapott Movable-t átadva (arrive()-ot lásd fentebb),
	 * majd jelezzük a mérlegnek (ha van rajta) az új összsúlyt.
	 * @param newLocation A mezo, amire lep.
	 * @param direction Az irany, amerre megy.
	 * @param movable A mozgatható objektum.
	 */
	public void leave(Tile newLocation, Direction direction, Movable movable) {
		characterWeight -= movable.getWeight();
		if(balance == null) {
			movable.setLocationBool(this, false);
			newLocation.arrive(movable);
		} else {
			//Ha az ajtó, ami a mezőben megegyezik azzal, amit a mezőben levő mérleg nyit, nem mehet át az ezredes (mert rázáródna)
			if(!balance.getDoor().equals(sides.get(direction))) {
				movable.setLocationBool(this, false);
				newLocation.arrive(movable);
				balance.weightChanged(boxFullWeight() + characterWeight);
			}
		}
	}
	
	/**
	 * Egy karakter dobozt akar felvenni innen, ezt hívja. Ha a Stack nem üres, a legfelsőt adjuk neki, ha üres 0 súlyút
	 * (giveBox(), lásd Character), majd jelezzük a mérlegnek (ha van) az új összsúlyt (weightChanged(), lásd Balance).
	 * @param character A karakter.
	 */
	public void gatherBox(Character character) {
		if(!box.empty()){
			character.giveBox(box.pop());
			if(balance != null) {
				balance.weightChanged(boxFullWeight());
			}
		} else {
			character.giveBox(0);
		}
	}
	
	/** 
	 * Egy karakter dobozt akar elhelyezni itt. Elvesszük a dobozt (takeBox(), lásd Charakter), felrakjuk a Stack tetejére (súlyát visszaadja a takeBox()) és
	 * megmondjuk a mérlegnek (ha van) az új összsúlyt (weightChanged(), lásd Balance).
	 * @param character A karakter.
	 * @param weight A súly.
	 */
	public void placeBox(Character character) {
		box.push(character.takeBox());
		if(balance != null) {
			balance.weightChanged(boxFullWeight());
		}
	}
	
	/**
	 * Karakter hívja, ha változik a súlya (dobozt felvesz, lerak), paraméterben átadja, hogy mennyivel változott a súlya.
	 * A mérlegnek (ha van) megkell mondani az új összsúlyt (weightChanged(), lásd Balance).
	 * @param weight A súly.
	 */
	public void weightChanged(int weight) {
		characterWeight += weight;
		if(balance != null) {
			balance.weightChanged(boxFullWeight() + characterWeight);
		}
	}
	
	/**
	 * A kimeneti nyelvnek megfelelően kiírja az adatokat.
	 */
	public void showStateTile() {
		//<azonosító>  <típus>  <Oneill> <Jaffa> <Replikátor> <ZPM> <doboz> <összsúly> <mérleg> <súlyhatár> <ajtó> <állapot>
		String balanceId = "null", balanceLimit = "null", doorId = "null", doorState = "null";
		if(balance != null) {
			balanceId = balance.getId();
			balanceLimit = Integer.toString(balance.getWeightLimit());
			doorId = balance.getDoor().getId();
			if(balance.getDoor().canMove(null)) {
				doorState = "open";
			} else {
				doorState = "closed";
			}
		}
		int fullWeight = boxFullWeight() + characterWeight;
		System.out.println(id + " " + type + " " + oneillStay + " " + jaffaStay + " " + replicatorStay + " " + zpm + " " + boxFullWeight()
		+ " " + fullWeight + " " + balanceId + " " + balanceLimit + " " + doorId + " " + doorState);
	}

	/**
	 * Viszaadja, hogy az ezredes a mezőn van-e.
	 * @return Igen/Nem.
	 */
	public boolean getOneill() {
		return oneillStay;
	}

	/**
	 * Beállítja, hogy az ezredes a mezőn van-e.
	 * @param oneillStay Igen/Nem.
	 */
	public void setOneill(boolean oneillStay) {
		this.oneillStay = oneillStay;
	}

	/**
	 * Viszaadja, hogy a jaffa a mezőn van-e.
	 * @return Igen/Nem.
	 */
	public boolean getJaffa() {
		return jaffaStay;
	}

	/**
	 * Beállítja, hogy a jaffa a mezőn van-e.
	 * @param jaffaStay Igen/Nem.
	 */
	public void setJaffa(boolean jaffaStay) {
		this.jaffaStay = jaffaStay;
	}

	/**
	 * Viszaadja, hogy a replikátor a mezőn van-e.
	 * @return Igen/Nem.
	 */
	public boolean getReplicator() {
		return replicatorStay;
	}

	/**
	 * Beállítja, hogy a replikátor a mezőn van-e.
	 * @param replicatorStay Igen/Nem.
	 */
	public void setReplicator(boolean replicatorStay) {
		this.replicatorStay = replicatorStay;
	}

	/**
	 *  Visszaadja van-e ZPM a mezőn.
	 * @return Van/Nincs.
	 */
	public Boolean getZpm() {
		return zpm;
	}
	
	/**
	 * Beállítja, hogy van-e ZPM a mezőn.
	 */
	public void setZpm(Boolean zpm) {
		this.zpm = zpm;
	}
	
	/**
	 * Visszaadja az azonosítót.
	 * @return Azonosító.
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Kiszámolja a dobozok összsúlyát.
	 */
	public int boxFullWeight() {
		int fullWeight = 0;
		for(int n=0;n<box.size();n++) {
			fullWeight += box.get(n);
		}
		return fullWeight;
	}
}
