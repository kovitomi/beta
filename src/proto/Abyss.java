package proto;

import java.util.Stack;

/** 
 * Szakadék. Tárolja a négy oldalánál mik vannak (Side, Wall, stb.), lelehet kérdezni (mint Tile-nál). Ha egy karakter rálép, megöli, doboz megsemmisül, ha beleejtik.
 *
 */
public class Abyss extends Tile {
	

	/**
	 * Jelzi, hogy át van-e alakítva mezővé (true) vagy nem (false). Alap értéke false.
	 */
	private Boolean isConverted = false;
	
	/**
	 * Meghívja a Tile construktort, és beállítja az isConverted-et false-ra.
	 * @param id A mező azonosítója.
	 * @param zpm Van-e a mezőn zpm.
	 * @param box A doboz stack, ami a mezőn van.
	 */
	public Abyss(String id, boolean zpm, Stack<Integer> box) {
		super(id, zpm, box);
		isConverted = false;
		type = "abyss";
	}
	
	/**
	 * Szakadékot mezővé alakítja (átbillenti az isConverted-et true-ba).
	 */
	public void convert() {
		isConverted = true;
		type = "abyssConverted";
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: ha isConverted false, az érkezőn (paraméterben adott) meghívjuk a die()-t (lásd Movable) egyébként az ős függvényét hívjuk meg.
	 * @param movable A mozgatható objektum, ami megérkezik a mezőre.
	 */
	@Override
	public void arrive(Movable movable) {
		if(isConverted == false) {
			movable.setLocation(this);
			movable.die(this);
		}else{
			super.arrive(movable);
		}
	}
	
	/**
	 * Felüldefiniáljuk az örököltet: ha isConverted false, a dobozt elvesszük (takeBox(), lásd Character), de nem tesszük fel (az örökölt) Stack-re (megsemmisül)
	 * egyébként az ős függvényét hívjuk meg.
	 * @param character A karakter, akinél a doboz van.
	 * @param weight A doboz súlya.
	 */
	@Override
	public void placeBox(Character character) {
		if(!isConverted) {
			character.takeBox();
		}else{
			super.placeBox(character);
		}
	}
	
	
}
