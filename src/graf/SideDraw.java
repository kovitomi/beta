package graf;

import java.awt.Graphics;

/**
 * Egy oldal kirajzolását végzi.
 * 
 */
public class SideDraw extends Drawable {
	
	/**
	 * Referencia a kirajzolandó oldalra.
	 */
	private Side side;
	
	/**
	 * Konstruktor.
	 * @param side Az oldal.
	 */
	public SideDraw(Side side) {
		this.side = side;
	}
	
	/**
	 * Kirajzolja az adott oldalt.
	 */
	public void draw(Graphics g) {
		String imageNameDirection;
		if(side.getTile(Direction.North) == null){
			imageNameDirection = "horizontal";
		}else{
			imageNameDirection = "vertical";
		}
		String imageNameSide = "side_" + imageNameDirection;
		
		g.drawImage(imageHandler.getImage("tile"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		g.drawImage(imageHandler.getImage(imageNameSide), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
	}
}
