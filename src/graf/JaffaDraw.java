package graf;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

/** 
 * A jaffa kirajzolását végzi.
 *
 */
public class JaffaDraw extends MovableDraw {
	
	/**
	 * Referencia a jaffa-ra.
	 */
	private Jaffa jaffa;
	
	/**
	 * Konstruktor.
	 */
	public JaffaDraw(Jaffa jaffa) {
	    super(jaffa);
	    this.jaffa = jaffa;
	}
	
	/**
	 * Meghatározza a pozícióját (getPosition, lásd MovableDaw), majd oda kirajzolja magát.
	 */
	public void draw(Graphics g) {
		String imageName = "jaffa_" + jaffa.getDirection().toString().toLowerCase();
		pos = getPosition();
		if(pos != null) {
		    g.drawImage(imageHandler.getImage(imageName), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		}
		
		String boxWeightString = "-";
		if(jaffa.boxWeight > 0) boxWeightString = jaffa.boxWeight + " kg";
		
		// Információs sávon az adatok
		Dimension windowSize = jaffa.game.getWindow().getSize();
		int infoPaneX = (int)windowSize.getWidth()-imageHandler.scaleOnlyWindow(200);
        g.setColor(java.awt.Color.white);
        g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(36)));
        g.drawString("Jaffa", infoPaneX+imageHandler.scaleOnlyWindow(50), imageHandler.scaleOnlyWindow(420));
        g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(25)));
        g.drawString("Súly: " + jaffa.getWeight() + " kg", infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(450));
        g.drawString("Doboz: " + boxWeightString, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(480));
        g.drawString("Zpm: " + jaffa.zpm, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(510));
        g.drawString("Kell: " + jaffa.maxzpm, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(540));
	}
}
