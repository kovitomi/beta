package graf;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

/**
 * A replicator kirajzolását végzi.
 */
public class ReplicatorDraw extends MovableDraw {
	
	/**
	 * Referencia a replicatorra.
	 */
	private Replicator replicator;
	
	/**
	 * Konstruktor.
	 * @param replicator A replikátor.
	 */
	public ReplicatorDraw(Replicator replicator) {
	    super(replicator);
	    this.replicator = replicator;
	}
	
	/**
	 * Meghatározza a pozícióját (getPosition, lásd MovableDaw), majd oda kirajzolja magát.
	 */
	public void draw(Graphics g) {
		pos = getPosition();
		if(replicator.isAlive() && pos != null) {
		    g.drawImage(imageHandler.getImage("replicator"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		}
		
		String aliveString = "nem";
		if(replicator.alive) aliveString = "igen";
		
		// Információs sávon az adatok
		Dimension windowSize = replicator.game.getWindow().getSize();
		int infoPaneX = (int)windowSize.getWidth()-imageHandler.scaleOnlyWindow(200);
        g.setColor(java.awt.Color.white);
        g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(36)));
        g.drawString("Replikátor", infoPaneX+imageHandler.scaleOnlyWindow(5), imageHandler.scaleOnlyWindow(660));
        g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(25)));
        g.drawString("Él még: " + aliveString, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(690));
	}
}
