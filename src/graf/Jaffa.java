package graf;

/**
 * A jaffa. A jaffát képviseli.
 *
 */
public class Jaffa extends Character {

	/**
	 * Knstruktor. A paramétereket a megfelelő attribútumoknak adjuk értékül.
	 * @param game A játék.
	 * @param selfWeight Az ezredes súlya.
	 * @param boxWeight A doboz súlya.
	 * @param zpm A megszerzett zpm-ek száma.
	 * @param maxzpm Az összegyűtendő zpm-ek száma.
	 * @param viewDirection Az irány, amerre néz.
	 */
	public Jaffa(Game game, int selfWeight, int boxWeight, int zpm, int maxzpm, Direction viewDirection){
		this.game = game;
		this.selfWeight = selfWeight;
		this.boxWeight = boxWeight;
		this.zpm = zpm;
		this.maxzpm = maxzpm;
		this.viewDirection = viewDirection;
	}
	
	/**
	 * A kimeneti nyelv alapján kiírja az adatokat (típus: Jaffa).
	 */
	public void showStateMovable() {
		//<típus> <súly> <doboz> <zpm> <maxzpm> <mező> <irány>
		System.out.println("Jaffa " + selfWeight + " " + boxWeight + " " + zpm + " " + maxzpm + " " + location.getId() + " " + viewDirection);
	}
	
	/**
	 * Meghal. Szól a Game-nek (jaffaLose()).
	 */
	public void die(Abyss abyss) {
		this.game.jaffaLose();
	}
	
	/**
	 * Növeli a zpm-ek értékét, true-t ad vissza, ezzel jelezve a mezőnek, hogy felvette.
	 * @return Igen/Nem.
	 */
	public boolean pickUpZpm() {
		zpm++;
		if(zpm == maxzpm) game.jaffaVictory();
		return true;
	}
	
	/**
	 * A mező hívja, beállítja hogy a jaffa az adott mezőn van-e.
	 * @param tile A mező.
	 * @param state Az adott mezőn van-e.
	 */
	public void setLocationBool(Tile tile, boolean state){
		tile.setJaffa(state);
	}
	
}
