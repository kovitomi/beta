package graf;

/**
 * Mozgatható objektum. Absztrakt osztály, a mozgásért felelős (aki tőle származik tud mozogni (a karakterek (az ezredes és a jaffa),a replikátor, van tartózkodási helye, súlya [ha a súly 0, az azt jelzi, hogy nincs]).
 *
 */
public abstract class Movable {

	/**
	 * Az irány, amerre éppen néz.
	 */
	protected Direction viewDirection = Direction.North;
	
	/**
	 * A tartózodási hely.
	 */
	protected Tile location = null;
	
	/**
	 * A súly.
	 */
	protected int selfWeight = 0;
	
	/**
	 * Játék kezelőre (Game) referencia.
	 */
	protected Game game = null;
	
	/**
	 * Pályára (Map) referencia.
	 */
	protected Map map = null;
	
	/**
	 * Adott irányba néz (4 égtáj lehet).
	 * @param direction Az irány, amerre nézne.
	 */
	public void look(Direction direction) {
		viewDirection = direction;
	}
	
	/**
	 * Visszadja hogy az adott mozgatható objektum merre néz.
	 * @return Az irány.
	 */
	public Direction getDirection(){
		return this.viewDirection;
	}
	
	/**
	 * Adott irányban megpróbál a következő (szomszédos) mezőre lépni: a nézési irány az adott irány lesz (look()), a tartózkodási helytől
	 * (ami egy mező) lekéri az adott irányban (paraméterben kapja) az odalt (Side), amin ha átlehet menni (Side-on canMove() true-t ad)
	 * lekéri az adott irányan a mezőt, majd meghívja a tartózkodási helyén a leave()-et, az új helyen az arrive()-ot (lásd Tile),
	 * ami beállítja az új tartózkodás helyet (setLocation()) és ha van rajta ZPM a pickUpZpm()-et vagy meghívja a die()-t (ha szakadék volt).
	 * @param direction Az irány, amerre mozogna.
	 */
	public void move(Direction direction) {
		Tile oldtile = this.location;
		look(direction);
		Side inWay = location.tileSide(direction);
		if(inWay.canMove(this)) {
			Tile newLocation = inWay.getTile(direction);
			location.leave(newLocation, direction, this);
		}
		oldtile.showStateTile();
		location.showStateTile();
		showStateMovable();
		game.isEnded();
	}
	
	/**
	 * A paraméterben kapott mezőt beállítja új tartózkodási helynek.
	 * @param tile A mező, amire megrkezett.
	 */
	public void setLocation(Tile tile) {
		location = tile;
	}
	
	/**
	 * Visszaadja a tartózkodási helyet
	 * @return Tartózkodási hely.
	 */
	public Tile getLocation() {
		return location;
	}
	/**
	 * Visszaadja a súlyát (karakterben felül kell definiálni, hogy hozzáadja még a doboz súlyát is).
	 * @return A saját súlya.
	 */
	public int getWeight() {
		return selfWeight;
	}
	
	/**
	 * A mező hívja, amire érkezik (lásd arrive(), Tile)  jelzi, hogy van rajta ZPM.
	 * @return Igen/Nem.
	 */
	public abstract boolean pickUpZpm();
	
	/**
	 * Szakadék (Abyss) hívja, ha belelép.
	 * @param abyss A szakadék, amire lépett.
	 */
	public abstract void die(Abyss abyss);
	
	/**
	 * A paraméterben kapott térképet beállítja.
	 * @param map A térkép.
	 */
	public void setMap(Map map){
		this.map = map;
	}
	
	/**
	 * A mező hívja, beállítja hogy az adott mozgatható osztály az adott mezőn van-e.
	 * @param tile A mező.
	 * @param state Az adott mezőn van-e.
	 */
	public abstract void setLocationBool(Tile tile, boolean state);
	
	/**
	 * Absztrakt, kilistázza, az adott mozgatható objektum adatait.
	 */
	public abstract void showStateMovable();
	
}
