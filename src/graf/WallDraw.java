package graf;

import java.awt.Graphics;

/**
 * Egy fal kirajzolását végzi.
 */
public class WallDraw extends SideDraw {
	
	/**
	 * Referencia a kirajzolandó falra.
	 */
	private Wall wall;
	
	/**
	 * Konstruktor.
	 * @param wall A fal.
	 */
	public WallDraw(Wall wall) {
        super(null);
        this.wall = wall;
	}
	
	/**
	 * Kirajzolja a falat.
	 */
	public void draw(Graphics g) {
		String imageNameDirection;
		if(wall.getTile(Direction.North) == null && wall.getTile(Direction.South) == null ){
			imageNameDirection = "vertical";
		}else{
			imageNameDirection = "horizontal";
		}
		String imageNameWall = "wall_" + imageNameDirection;
		
		g.drawImage(imageHandler.getImage(imageNameWall), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
	}
}
