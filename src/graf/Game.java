package graf;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import javax.swing.*;
import java.util.ArrayList;




/** 
 * Jatek. A játék kezeléséért, felhasználóval kapcsolat tartásért, inicializálásáért felelős.
 *
 */
public class Game {
	
	/**
	 * Az ezredesre referencia.
	 */
	private Colonel oneill = null;
	
	/**
	 * A jaffára referencia.
	 */
	private Jaffa jaffa = null;
	
	/**
	 * Az éppen élő replikátorra referencia.
	 */
	private Replicator replicator = null;
	
	/**
	 * Referencia a pályára.
	 */
	private Map map = null;
	
	/**
	 * Teszt-e.
	 */
	private Boolean isTest = false;
	
	/**
	 * Tároljuk, hogy a játék véget ért-e, illetve hogy ki nyerte.
	 */
	private Boolean ended = null;
	
	/**
	 * Heterogén kollekció a pálya elemeiről. 
	 * Mezők, oldalak, karakterek (Oneill, Jaffa, Replikátor) sorrendben kell tárolni.
	 */
	private ArrayList<Drawable> objects = null;
	
	/**
	 * Tároljuk, hogy válaszotott-e már a felhasználó pályát.
	 */
	private String mapSelected = "";
	
	/**
	 * A mezők (Tile) pozíciót tartalmazza, a mezők azonosítójával (id) címezhető. 
	 */
	private LinkedHashMap<String, Position> tilesLocation = null;
		
	/**
	 * ImageHandler.
	 */
	private ImageHandler imageHandler;
	
	/**
	 * Segédváltozó az inicializáláshoz. A modell objektum id-jével lehet címezni a rajzoló objektumokat.
	 * A 64x128 vagy 128x64 méretűek kerülnek bele (pl. Wall).
	 */
	private LinkedHashMap<String, Drawable> small;
	
	/**
	 * Segédváltozó az inicializáláshoz. A modell objektum id-jével lehet címezni a rajzoló objektumokat.
	 * A 128x128 méreűek kerülnek bele (pl. Side).
	 */
	private LinkedHashMap<String, Drawable> big;
	
	/**
	 * A képernyő méretezésének aránya.
	 */
	private double windowScale = 1;
	
	/**
	 * A játékmező méretezésének aránya.
	 */
	private double mapScale = 1;
	
	/**
	 * A méretezés nélküli ablakszélesség.
	 */
	private int defaultWidth = 1096;
	
	/**
	 * A méretezés nélküli ablakmagasság.
	 */
	private int defaultHeight = 896;
	
	/**
	 * Segédváltozó: Abyss ID -> Abyss
	 */
	private LinkedHashMap<String, Abyss> abyssIds;
	
	/**
	 * A kirajzolandó ablak.
	 */
	private JFrame window = null;
	
	/**
	 * A menühöz tartozó pályák listája.
	 */
	private JList<String> list = null;
	
	/**
	 * A pályák a pályanevekkel és fájlnevkkel.
	 */
	private LinkedHashMap<String, String> maps = null;
	
	/**
	 * A játékmezőt reprezentáló panel.
	 */
	private JPanel bgPanel = null;
	
	/**
	 * Segédváltozó a replikátor mozgásához, lefutott-e az initialize.
	 */
	private boolean mapInitialized = false;
	
	/**
	 * Segédváltozó a replikátor mozgásához.
	 */
	private boolean timer = true;
	
	/**
	 * Segédváltozó a replikátor mozgásához, az időzítő.
	 */
	Timer randomMove;
	
	/**
	 * Kezeli az ablakokat: ő fogadja és dolgozza fel a parancsokat. Játék indításakor meghívja az initialize()-t.
	 * @param args
	 */
	public static void main(String[] args) throws IOException {	
		Game game = new Game();
		game.inputHandling();
	}
	
	/**
	 * Lekezeli a bementi parancsokat, és minden esetben elindítja a megfelelő mechanizmust.
	 */
	public void inputHandling() throws IOException{
		
		if(mapSelected.isEmpty()){
			// Background
			java.net.URL imgURL = getClass().getResource("img/menu.png");
		    
		    // Map List
		    maps = new LinkedHashMap<String, String>();
		    maps.put("Pálya 1", "map01");
		    maps.put("Pálya 2", "map02");
		    maps.put("Pálya 3", "map03");
		    maps.put("Pálya 4", "map04");
		    maps.put("Pálya 5", "map05");
		    
		    // Scroller List
		    DefaultListModel<String> listModel = new DefaultListModel<String>();
		    for (String value : maps.keySet())
		    {
		    	listModel.addElement(value);
		    }
		    list = new JList<String>(listModel);
		    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		    list.setLayoutOrientation(JList.VERTICAL);
		    list.setSelectedIndex(0);
		    list.setFont(list.getFont().deriveFont(24.0f));
		    JScrollPane listScroller = new JScrollPane(list);
		    listScroller.setPreferredSize(new Dimension(105, 140));
		    
		    //Klikkelés kezelése a listában. A kattintással el lehet indítani egy játékot.
		    MouseListener mouseListener = new MouseAdapter() {
		        public void mouseClicked(MouseEvent e) {
		            if (e.getClickCount() == 1) {
		            	mapSelected = maps.get(list.getSelectedValue());
		            	java.net.URL url = getClass().getResource("maps/" + mapSelected + ".txt");
		            	File file = new File(url.getPath());
		            	try {
							initialize(file);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
		             }
		        }
		    };
		    list.addMouseListener(mouseListener);
		    		    
		    // Creating menu window
			window = new JFrame("PrjoLab menu");
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    int windowBorderX = 6;
		    int windowBorderY = 29;
			window.setSize(300+windowBorderX,300+windowBorderY);
			window.setLocationRelativeTo(null);
			window.setContentPane(new JLabel(new ImageIcon(imgURL)));
			window.setLayout(new FlowLayout(FlowLayout.LEFT, 100, 0));

		    JButton b1=new JButton("Kilépés");
		    b1.setPreferredSize(new Dimension(105, 30));
		    
		    // Eseménykezelők. ESC és Enter billentyűk lekezelése.
		    ActionListener actionListenerButton = new ActionListener() {
		        public void actionPerformed(ActionEvent e)
		        {
		        	window.dispose();
		        }
		    };
		    KeyListener keyListenerButton = new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if(e.getKeyChar() == KeyEvent.VK_ESCAPE || e.getKeyChar() == KeyEvent.VK_ENTER) {
		            	window.dispose();
		            }
				}
		    };
		    KeyListener keyListenerWindow = new KeyAdapter() {
				public void keyPressed(KeyEvent e) {
					if(e.getKeyChar() == KeyEvent.VK_ESCAPE) {
		            	window.dispose();
		            }
					if(e.getKeyChar() == KeyEvent.VK_ENTER || e.getKeyChar() == KeyEvent.VK_SPACE) {
						mapSelected = maps.get(list.getSelectedValue());
		            	java.net.URL url = getClass().getResource("maps/" + mapSelected + ".txt");
		            	File file = new File(url.getPath());
		            	try {
							initialize(file);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
		    };
		    
		    window.addKeyListener(keyListenerWindow);
		    list.addKeyListener(keyListenerWindow);
		    b1.addKeyListener(keyListenerButton);
		    b1.addActionListener(actionListenerButton);
		    		    
		    // Empty border szürke hátteret generálna, ezért egy üres nem látszó labelre rakjuk.
		    JLabel invisible = new JLabel();
		    invisible.setBorder(BorderFactory.createEmptyBorder(0, 300, 65, 300));
		    
		    // Adding elements
		    window.add(invisible);
		    window.add(listScroller);
		    window.add(b1);
		    
		    window.setResizable(false);
		    window.setVisible(true);
	    }else{
	    	window.dispose();
		    window = new JFrame("PrjoLab game");
		    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    int windowBorderX = 6;
		    int windowBorderY = 29;
		    window.setSize(scaleWindow(defaultWidth)+windowBorderX,scaleWindow(defaultHeight)+windowBorderY);
		    window.setLocationRelativeTo(null);
		    window.setResizable(false);

			drawAll();
	    }
	}
	
	
	/**
	 * Létrehozza a Portalt, majd a mezőket (Tile), oldalakat (Side), karaktereket (Char, Character), replikátort (Repl, Replicator) hozzuk létre. Mezőket,
	 * oldalakat osztályonként különbőző LinkedHashMap-be rakjuk, az azonosítójukat listába (pl. ArrayList). Beállítjuk a mezőkben az oldalak referenciáit,
	 * az oldalakban a mezők referenciáit (Conn). Létrehozzuk a mezőket (Bal, Balance) LinkedHashMap-ben, beállítjuk az ajtókra (Door) a referenciákat,
	 * az ajtókban pedig a mérlegekre. A pályán elhelyezzük az ezredest, a jaffát és a replikátort. Létrehozzuk a Map-et és átadjuk neki az objektumokat.
	 * A grafikus objektumokat is itt hozzuk létre, kicsi és nagy szerint csoportosítva tároljuk annak a objektumnak az azonosítójával címezhetővé téve,
	 * amelyiket kikell rajzolnia.
	 * @param map A térkép fájl.
	 * @throws IOException 
	 */
	public void initialize(File map) throws IOException {
		Scanner scanner = null;
		try {
			scanner = new Scanner(map);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		String line = "";
		boolean scanning = true;
		String[] temp1;
		String[] temp2;
		
		//Tile ID -> Tile
		LinkedHashMap<String, Tile> tiles = new LinkedHashMap<String, Tile>();
		
		//Side ID -> Side
		LinkedHashMap<String, Side> sides = new LinkedHashMap<String, Side>();
		
		//Mérleg ID -> Mérleg
		LinkedHashMap<String, Balance> balances = new LinkedHashMap<String, Balance>();
		
		//Tile ID -> Balance ID
		LinkedHashMap<String, String> balancePlaces = new LinkedHashMap<String, String>();
		
		//Tile ID -> BalanceDraw
		LinkedHashMap<String, BalanceDraw> balanceDraws = new LinkedHashMap<String, BalanceDraw>();
		
		small = new LinkedHashMap<String, Drawable>();
		big = new LinkedHashMap<String, Drawable>();
		objects = new ArrayList<Drawable>();
		tilesLocation = new LinkedHashMap<String, Position>();
		imageHandler = new ImageHandler(this);
		abyssIds = new LinkedHashMap<String, Abyss>();
                
		String oniellPlace = "";
		String jaffaPlace = "";
		String replicatorPlace = "";
		String cornerTile = "";
		
		/**
		 * A mapx.txt beolvasását végző ciklus. Végigmegy az egész fájlon,
		 * és létrehozza a megfelelő objektumokat.
		 */
		while(scanning){
			line = scanner.nextLine();
			temp1 = line.split("-");
			Stack<Integer> box = new Stack<Integer>();
			Direction dir = Direction.North;
			
			if(temp1[0] != null){
				switch(temp1[0]){
				// Mezők beolvasása.
				case "Tile":
					temp2 = temp1[1].split(",");
					if(Integer.parseInt(temp2[1]) == 1){
						Abyss abyss = new Abyss(temp2[0], false, new Stack<Integer>());
						tiles.put(temp2[0], abyss);
						abyssIds.put(temp2[0], abyss);
					} else {
						if(Integer.parseInt(temp2[3]) != 0) {
						    box.push(Integer.parseInt(temp2[3]));
						}
						Boolean zpm = false;
						if(Integer.parseInt(temp2[2])==1) zpm = true;
						Tile tile = new Tile(temp2[0], zpm, box);
						tiles.put(temp2[0], tile);
					}
					break;
				// Oldalak beolvasása.
				case "Side":
					temp2 = temp1[1].split(",");
					switch(temp2[1]){
					case "wall":
						Wall wall = new Wall(temp2[0]);
						sides.put(temp2[0], wall);
						small.put(temp2[0], new WallDraw(wall));
						break;
					case "open":
						Side side = new Side(temp2[0]);
						sides.put(temp2[0], side);
						big.put(temp2[0], new SideDraw(side));
						break;
					case "spec":
						SpecialWall spec = new SpecialWall(temp2[0]);
						sides.put(temp2[0], spec);
						small.put(temp2[0], new SpecialWallDraw(spec));
						break;
					case "door":
						Door door = new Door(temp2[0]);
						sides.put(temp2[0], door);
						big.put(temp2[0], new DoorDraw(door));
						break;
					}
					break;
				// Ezredes és Jaffa beolvasása.
				case "Char":
					temp2 = temp1[1].split(",");
					switch(temp2[6]){
					case "North":
						dir = Direction.North;
						break;
					case "South":
						dir = Direction.South;
						break;
					case "West":
						dir = Direction.West;
						break;
					case "East":
						dir = Direction.East;
						break;
					}
					switch(temp2[0]){
					case "Colonel":
						oneill = new Colonel(this,Integer.parseInt(temp2[2]),Integer.parseInt(temp2[3]),Integer.parseInt(temp2[5]),Integer.parseInt(temp2[4]), dir);
						oniellPlace = temp2[1];
						break;
					case "Jaffa":
						jaffa = new Jaffa(this,Integer.parseInt(temp2[2]),Integer.parseInt(temp2[3]),Integer.parseInt(temp2[5]),Integer.parseInt(temp2[4]), dir);
						jaffaPlace = temp2[1];
						break;
					}
					break;
				// Mérlegek beolvasása.
				case "Bal":
					temp2 = temp1[1].split(",");
					balances.put(temp2[0], new Balance(temp2[0], (Door)sides.get(temp2[2]), Integer.parseInt(temp2[1])));
					balancePlaces.put(temp2[3], temp2[0]);
					break;
				// A mezők és oldalak közti kapcsolatok beolvasása.
				case "Conn":
					temp2 = temp1[1].split(",");
					LinkedHashMap<Direction, Side> sidesForTile = new LinkedHashMap<Direction, Side>();
					sidesForTile.put(Direction.North, sides.get(temp2[1]));
					sidesForTile.put(Direction.West, sides.get(temp2[2]));
					sidesForTile.put(Direction.East, sides.get(temp2[3]));
					sidesForTile.put(Direction.South, sides.get(temp2[4]));
					tiles.get(temp2[0]).setSides(sidesForTile, balances.get(balancePlaces.get(temp2[0])));
					if(balances.get(balancePlaces.get(temp2[0])) != null) {
						BalanceDraw balanceDraw = new BalanceDraw(balances.get(balancePlaces.get(temp2[0])));
						balanceDraw.setImageHandler(imageHandler);
						balanceDraws.put(temp2[0], balanceDraw);
					}
					sides.get(temp2[1]).setTiles(Direction.South, tiles.get(temp2[0]));
					sides.get(temp2[2]).setTiles(Direction.East, tiles.get(temp2[0]));
					sides.get(temp2[3]).setTiles(Direction.West, tiles.get(temp2[0]));
					sides.get(temp2[4]).setTiles(Direction.North, tiles.get(temp2[0]));					
					break;
				// A replicator beolvasása.
				case "Replicator":
					temp2 = temp1[1].split(",");
					switch(temp2[3]){
					case "North":
						dir = Direction.North;
						break;
					case "South":
						dir = Direction.South;
						break;
					case "West":
						dir = Direction.West;
						break;
					case "East":
						dir = Direction.East;
						break;
					}
					replicator = new Replicator(this, dir);
					replicatorPlace = temp2[1];
					break;
				case "Corner":
					cornerTile = temp1[1];
					break;
				case "ScaleMap":
					int mapSize = Integer.parseInt(temp1[1]);
					int mapCells128 = mapSize*2+1;
					int actualSize = 128*mapCells128;
					mapScale = (double) defaultHeight / (double) actualSize;
					break;
				case "ScaleWindow":
					temp2 = temp1[1].split(",");
					windowScale = (double) Integer.parseInt(temp2[0]) / (double) Integer.parseInt(temp2[1]);
					break;
				default:
					System.out.println("A megadott típus nem értelmezhető.");
					break;
				}
			}
							
			if(!scanner.hasNextLine()){
				scanning = false;
				
				//Az ezredes beállítása:
				oneill.setLocation(tiles.get(oniellPlace));
				tiles.get(oniellPlace).setOneill(true);
				tiles.get(oniellPlace).weightChanged(oneill.getWeight());
				
				//A jaffa beállítása:
				jaffa.setLocation(tiles.get(jaffaPlace));
				tiles.get(jaffaPlace).setJaffa(true);
				tiles.get(jaffaPlace).weightChanged(jaffa.getWeight());
				
				//A replikátor beálllítása
				replicator.setLocation(tiles.get(replicatorPlace));
				tiles.get(replicatorPlace).setReplicator(true);
				
				//Pálya kezelő létrehozása és beállítása, karaktereknek odaadása:
				this.map = new Map((Colonel)oneill, (Jaffa)jaffa, replicator, new Portal(), this, tiles, sides, balances, isTest);
				oneill.setMap(this.map);
				jaffa.setMap(this.map);
				replicator.setMap(this.map);
				
				//Rajzoló objektumok beállítása (pozíció):
				initDrawables(tiles.get(cornerTile), 128, 128, balanceDraws);
				
				//Ezredes rajzoló objektuma:
				ColonelDraw oneillDraw = new ColonelDraw(oneill);
				oneillDraw.setImageHandler(imageHandler);
				oneillDraw.setGame(this);
				objects.add(oneillDraw);
				
				//Jaffa rajzoló objektuma:
				JaffaDraw jaffaDraw = new JaffaDraw(jaffa);
				jaffaDraw.setImageHandler(imageHandler);
				jaffaDraw.setGame(this);
				objects.add(jaffaDraw);
				
				//Replikátor rajzolü objektuma:
				ReplicatorDraw replicatorDraw = new ReplicatorDraw(replicator);
				replicatorDraw.setImageHandler(imageHandler);
				replicatorDraw.setGame(this);
				objects.add(replicatorDraw);
				
				mapInitialized = true;
				
				//Jaták indítása:
				inputHandling();
			}
		}
	}
	
	/**
	 * Beállítja a mezők és oldalak pozícióját, a bal-felső sarokhoz viszonyítva (rekurzívan). Minden objektumok hamár beállítottunk,
	 * kiveszünk az ideiglenes tárolóból (big és small), így arra nem megyünk még egyszer. A mezőket berakjuk a big-be.
	 * @param tile A mező, ahonnan indul.
	 * @param x Az induló x koordináta.
	 * @param y Az induló y koordináta.
	 */
	public void initDrawables(Tile tile, int x, int y, LinkedHashMap<String, BalanceDraw> balanceDraws) {
		if(tile == null) return;
		Drawable drawable;
		//Először Északra megyünk
		if(big.containsKey(tile.tileSide(Direction.North).getId())) {
			drawable = big.get(tile.tileSide(Direction.North).getId());
			drawable.setPosition(x, y-128);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(false);
			objects.add(drawable);
			big.remove(tile.tileSide(Direction.North).getId());
			if(tile.tileSide(Direction.North).getTile(Direction.North) != null) {
				initDrawables(tile.tileSide(Direction.North).getTile(Direction.North), x, y-256, balanceDraws);
			}
		}
		if(small.containsKey(tile.tileSide(Direction.North).getId())) {
			drawable = small.get(tile.tileSide(Direction.North).getId());
			drawable.setPosition(x, y-64);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(false);
			objects.add(drawable);
			small.remove(tile.tileSide(Direction.North).getId());
			if(tile.tileSide(Direction.North).getTile(Direction.North) != null) {
				initDrawables(tile.tileSide(Direction.North).getTile(Direction.North), x, y-256, balanceDraws);
			}
		}
		//Ha arra nem tudunk tovább, akkor Nyugatra
		if(big.containsKey(tile.tileSide(Direction.West).getId())) {
			drawable = big.get(tile.tileSide(Direction.West).getId());
			drawable.setPosition(x-128, y);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(true);
			objects.add(drawable);
			big.remove(tile.tileSide(Direction.West).getId());
			if(tile.tileSide(Direction.West).getTile(Direction.West) != null) {
				initDrawables(tile.tileSide(Direction.West).getTile(Direction.West), x-256, y, balanceDraws);
			}
		}
		if(small.containsKey(tile.tileSide(Direction.West).getId())) {
			drawable = small.get(tile.tileSide(Direction.West).getId());
			drawable.setPosition(x-64, y);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(true);
			objects.add(drawable);
			small.remove(tile.tileSide(Direction.West).getId());
			if(tile.tileSide(Direction.West).getTile(Direction.West) != null) {
				initDrawables(tile.tileSide(Direction.West).getTile(Direction.West), x-256, y, balanceDraws);
			}
		}
		//Majd Délre
		if(big.containsKey(tile.tileSide(Direction.South).getId())) {
			drawable = big.get(tile.tileSide(Direction.South).getId());
			drawable.setPosition(x, y+128);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(false);
			objects.add(drawable);
			big.remove(tile.tileSide(Direction.South).getId());
			if(tile.tileSide(Direction.South).getTile(Direction.South) != null) {
				initDrawables(tile.tileSide(Direction.South).getTile(Direction.South), x, y+256, balanceDraws);
			}
		}
		if(small.containsKey(tile.tileSide(Direction.South).getId())) {
			drawable = small.get(tile.tileSide(Direction.South).getId());
			drawable.setPosition(x, y+128);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(false);
			objects.add(drawable);
			small.remove(tile.tileSide(Direction.South).getId());
			if(tile.tileSide(Direction.South).getTile(Direction.South) != null) {
				initDrawables(tile.tileSide(Direction.South).getTile(Direction.South), x, y+256, balanceDraws);
			}
		}
		//Majd Keletre
		if(big.containsKey(tile.tileSide(Direction.East).getId())) {
			drawable = big.get(tile.tileSide(Direction.East).getId());
			drawable.setPosition(x+128, y);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(true);
			objects.add(drawable);
			big.remove(tile.tileSide(Direction.East).getId());
			if(tile.tileSide(Direction.East).getTile(Direction.East) != null) {
				initDrawables(tile.tileSide(Direction.East).getTile(Direction.East), x+256, y, balanceDraws);
			}
		}
		if(small.containsKey(tile.tileSide(Direction.East).getId())) {
			drawable = small.get(tile.tileSide(Direction.East).getId());
			drawable.setPosition(x+128, y);
			drawable.setImageHandler(imageHandler);
			drawable.setVertical(true);
			objects.add(drawable);
			small.remove(tile.tileSide(Direction.East).getId());
			if(tile.tileSide(Direction.East).getTile(Direction.East) != null) {
				initDrawables(tile.tileSide(Direction.East).getTile(Direction.East), x+256, y, balanceDraws);
			}
		}
		//Mező hozzáadása az elejére:
		if(!big.containsKey(tile.getId())) {
			if(abyssIds.containsKey(tile.getId())) {
			    AbyssDraw abyssDraw = new AbyssDraw(abyssIds.get(tile.getId()));
			    abyssDraw.setImageHandler(imageHandler);
			    abyssDraw.setBalance(null);
			    Position position = new Position();
			    position.setX(x);
			    position.setY(y);
			    abyssDraw.setPosition(x, y);
			    tilesLocation.put(tile.getId(), position);
			    objects.add(abyssDraw);
			    big.put(tile.getId(), abyssDraw);
			} else {
			    TileDraw tileDraw = new TileDraw(tile);
			    tileDraw.setImageHandler(imageHandler);
			    tileDraw.setPosition(x, y);
			    Position position = new Position();
			    position.setX(x);
			    position.setY(y);
			    tilesLocation.put(tile.getId(), position);
			    if(balanceDraws.get(tile.getId()) != null) {
				balanceDraws.get(tile.getId()).setPosition(x, y);
			    }
			    tileDraw.setBalance(balanceDraws.get(tile.getId()));
			    objects.add(0, tileDraw);
			    big.put(tile.getId(), tileDraw);
			}
		}
	}
	
	/**
	 * Ha az ezredes meghal, ezt a függvényt hívja. Kiírja majd az isEnded-et hívva, hogy “Jaffa nyert!”.
	 */
	public void oneillLose() {
		ended = false;
		randomMove.stop();
	}
	
	/**
	 * Ha a jaffa meghal, ezt a függvényt hívja. Kiírja majd az isEnded-et hívva, hogy “Ezredes nyert!”.
	 */
	public void jaffaLose() {
		ended = true;
		randomMove.stop();
	}
	
	/**
	 * Ha az ezredes begyűjtött annyi ZPM-et, amennyit kell neki ezt a függvényt hívja. Kiírja  majd az isEnded-et hívva, hogy “Ezredes nyert!”.
	 */
	public void oneillVictory() {
		ended = true;
		randomMove.stop();
	}
	
	/**
	 * Ha a jaffa begyűjtött annyit ZPM-et, amennyit kell neki ezt a függvéynt hívja. Kiírja majd az isEnded-et hívva, hogy “Jaffa nyert!”.
	 */
	public void jaffaVictory() {
		ended = false;
		randomMove.stop();
	}
	
	/**
	 * Ezt kell hívni, ha a replikátor meghal.
	 */
	public void replicatorDied() {
		replicator = null;
		randomMove.stop();
	}
	
	/**
	 * Kiírja, ha a játék véget ért.
	 */
	public void isEnded() {
		if(ended != null && ended == false) System.out.println("Jaffa nyert!");
		if(ended != null && ended == true) System.out.println("Ezredes nyert!");
	}
	
	/**
	 * Képernyő kirajzolásakor kell hívni: kirajzolja a hátteret, végig iterál a heterogén kollekción (objects) és mindre meghívja a Draw()-t.
	 * Játék végekor szintén ő rajzolja ki a képernyőt, illetve itt van definiálva a billenytű kezelő KeyListener, a replikátor random mozgása.
	 */
	public void drawAll() throws IOException {
		KeyListener keyListener = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(ended == null){
					//Jaffa kezelése
					if (e.getKeyChar() == 'w') {
						jaffa.move(Direction.North);
					}
					if (e.getKeyChar() == 'a') {
						jaffa.move(Direction.West);
					}
					if (e.getKeyChar() == 's') {
						jaffa.move(Direction.South);
					}
					if (e.getKeyChar() == 'd') {
						jaffa.move(Direction.East);
					}
					if (e.getKeyChar() == 'q') {
						jaffa.look(jaffa.getDirection().getLeft());
					}
					if (e.getKeyChar() == 'e') {
						jaffa.look(jaffa.getDirection().getRight());
					}
					if (e.getKeyChar() == '1') {
						jaffa.shootStargate(Color.Green);
					}
					if (e.getKeyChar() == '2') {
						jaffa.shootStargate(Color.Red);
					}
					if (e.getKeyChar() == 'y') {
						jaffa.pickUpBox();
					}
					if (e.getKeyChar() == 'x') {
						jaffa.putDownBox();
					}
					
					//Oneill kezelése
					if (e.getKeyChar() == 'i') {
						oneill.move(Direction.North);
					}
					if (e.getKeyChar() == 'j') {
						oneill.move(Direction.West);
					}
					if (e.getKeyChar() == 'k') {
						oneill.move(Direction.South);
					}
					if (e.getKeyChar() == 'l') {
						oneill.move(Direction.East);
					}
					if (e.getKeyChar() == 'u') {
						oneill.look(oneill.getDirection().getLeft());
					}
					if (e.getKeyChar() == 'o') {
						oneill.look(oneill.getDirection().getRight());
					}
					if (e.getKeyChar() == '8') {
						oneill.shootStargate(Color.Blue);
					}
					if (e.getKeyChar() == '9') {
						oneill.shootStargate(Color.Orange);
					}
					if (e.getKeyChar() == 'n') {
						oneill.pickUpBox();
					}
					if (e.getKeyChar() == 'm') {
						oneill.putDownBox();
					}
					try {
						drawAll();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}else{
					if (e.getKeyChar() == KeyEvent.VK_ENTER || e.getKeyChar() == KeyEvent.VK_SPACE){
						try {
							window.dispose();
							randomMove.stop();
							Game game = new Game();
							game.inputHandling();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
					}
				}
				if(e.getKeyChar() == KeyEvent.VK_ESCAPE) {
	            	window.dispose();
	            	randomMove.stop();
	            }
			}
		};
		
	    //Replicator random mozgás.
		int delay = 3000;
		ActionListener taskPerformer = new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	if(mapInitialized == true && replicator != null){
				    Random random = new Random();
		    	    int i = random.nextInt(4) + 1;
		            if(i == 1) replicator.move(Direction.North);
		    	    else if(i == 2) replicator.move(Direction.South);
		    	    else if(i == 3) replicator.move(Direction.West);
			        else if(i == 4) replicator.move(Direction.East);
			        randomMove.restart();
		            try {
		    	        drawAll();
		            } catch (IOException ex) {
		    	        ex.printStackTrace();
		            }
		    	}
		    }
		};
	    if(timer == true){
	    	timer = false;
	    	randomMove = new Timer(delay, taskPerformer);
	    	randomMove.start();
	    }
    
	    // Ha először hozzuk létre, akkor hozzáadjuk a keyListenert
		if(bgPanel != null){
			window.remove(bgPanel);
		}else{
		    window.addKeyListener(keyListener);
		}
		
		// Játéktér felrajzolása
	    bgPanel = new JPanel(new BorderLayout()) {{
	            setOpaque(false);
	        }
	        protected void paintComponent(Graphics g) {
	            super.paintComponent(g);
   
	    	    int innerX = scaleWindow(defaultWidth);
	    	    int innerY = scaleWindow(defaultHeight);
	    	    int singleTile = scaleMap(64);
	    	    int doubleTile = scaleMap(128);
	    	    
	            // Default fal background, így elkerüljük a kimaradó sarkokat, vagy nem érinettt részeket
	            for (int width = 0; width < innerX; width = width + singleTile) {
					for (int height = 0; height < innerY; height = height + doubleTile) {
						g.drawImage(imageHandler.getImage("wall_vertical"), width, height, null);
					}
				}
	            
	            // Az információs sáv (a tartalma az ezredes/jaffa/replikátor draw-jába található)
	            g.drawImage(imageHandler.getOnlyWindowScaledImage("information_pane"), innerY, 0, null);
	            
	            // Végigiterálunk a kirajzolandó objektumokon
	            for(int n=0;n<objects.size();n++) {
	            	objects.get(n).draw(g);
			    }
	            
	            // A játék vége képernyő
	            if(ended != null){
	            	g.drawImage(imageHandler.getOnlyWindowScaledImage("victory_screen"), scaleWindow(0), scaleWindow(0), null);
		    		if(ended == true){
		    			g.drawImage(imageHandler.getOnlyWindowScaledImage("victory_colonel"), scaleWindow(200), scaleWindow(338), null);
		    		}else{
		    			g.drawImage(imageHandler.getOnlyWindowScaledImage("victory_jaffa"), scaleWindow(200), scaleWindow(338), null);
		    		}
	            }
	        }	        
	    };
	    window.add(bgPanel);
	    window.setVisible(true);
	}
	
	/**
	 * Visszaadja az adott azonosítójú mező pozícióját.
	 * @param pos Az id.
	 */
	public Position getPosition(String pos) {
		return tilesLocation.get(pos);
	}
	
	/**
	 * Visszaadja az ablakot.
	 * @return Az ablak.
	 */
	public JFrame getWindow(){
		return window;
	}
	
	/**
	 * Ablak méretarány állítása.
	 */
	public int scaleWindow(int original){
		return (int) (original*windowScale);
	}
	
	/**
	 * Térkép méretarány állítása.
	 */
	public int scaleMap(int original){
		return (int) (original*windowScale*mapScale);
	}
	
	/**
	 * Az ablak méretarányát adja vissza.
	 */
	public double getWindowScale(){
		return windowScale;
	}
	
	/**
	 * A térkép méretarányát adja vissza.
	 */
	public double getMapScale(){
		return mapScale;
	}

}
