package graf;



/**
 * A képernyő egy pozícióját tárolja.
 * 
 */
public class Position {
	
	/**
	 * Az x koordináta.
	 */
	private int x;
	
	/**
	 * Az y koordináta. 
	 */
	private int y;
	
	/**
	 * Visszaadja az x koordinátát.
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Beállítja az x koordinátát.
	 */
	public void setX(int x_coordinate) {
		x = x_coordinate;
	}
	
	/**
	 * Visszaadja az y koordinátát.
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Beállítja az y koordinátát.
	 */
	public void setY(int y_coordinate) {
		y = y_coordinate;
	}
}
