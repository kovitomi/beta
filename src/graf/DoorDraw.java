package graf;

import java.awt.Graphics;

/** 
 * Egy ajtó kirajzolását végzi.
 *
 */
public class DoorDraw extends SideDraw {
	
	/** 
	 * Referencia a kirajzolandó ajtóra.
	 */
	private Door door;
	
	/** 
	 * Konstruktor.
	 * @param door Az ajtó.
	 */
	public DoorDraw(Door door) {
        super(null);
        this.door = door;
	}
	
	/** 
	 * Lekérdezi az ajtót, hogy nyitva van-e, ennek alapján rajzolja ki.
	 */
	public void draw(Graphics g) {
		String imageNamePermeable;
		if(door.permeable){
			imageNamePermeable = "open";
		}else{
			imageNamePermeable = "closed";
		}
		String imageNameDirection;
		if(door.getTile(Direction.North) == null){
			imageNameDirection = "horizontal";
		}else{
			imageNameDirection = "vertical";
		}
		String imageNameDoor = "door_" + imageNamePermeable + "_" + imageNameDirection;
		String imageNameSide = "side_" + imageNameDirection;
		
		g.drawImage(imageHandler.getImage("tile"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		g.drawImage(imageHandler.getImage(imageNameSide), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		g.drawImage(imageHandler.getImage(imageNameDoor), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
	}
}
