package graf;
import java.util.HashMap;
import javax.imageio.ImageIO;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * A képeket tőle kell elkérni kirajzoláshoz, megfelelően méretezi őket.
 */
public class ImageHandler {
	
	/**
	 * A képek a hozzájuk tartozó azonosítóval.
	 */
	private HashMap<String, BufferedImage> images;
	
	/**
	 * Referencia a játékra.
	 */
	private Game game = null;
	
	/**
	 * Konstruktor
	 * @param game A játékkezelő.
	 * @throws IOException 
	 */
	public ImageHandler(Game game) throws IOException {
		this.game = game;
		images = new HashMap<String, BufferedImage>();
		
		// Panes
		images.put("information_pane", ImageIO.read(getClass().getResource("img/information_pane.png")));
		images.put("victory_screen", ImageIO.read(getClass().getResource("img/victory_screen.png")));
		images.put("victory_colonel", ImageIO.read(getClass().getResource("img/victory_colonel.png")));
		images.put("victory_jaffa", ImageIO.read(getClass().getResource("img/victory_jaffa.png")));
		
		// Tile images
		images.put("tile", ImageIO.read(getClass().getResource("img/tile.png")));
		images.put("abyss", ImageIO.read(getClass().getResource("img/abyss.png")));
		images.put("balance", ImageIO.read(getClass().getResource("img/balance.png")));
		images.put("box", ImageIO.read(getClass().getResource("img/box.png")));
		images.put("zpm", ImageIO.read(getClass().getResource("img/zpm.png")));
		
		// Characters
		images.put("oneill_north", ImageIO.read(getClass().getResource("img/oneill_north.png")));
		images.put("oneill_west", ImageIO.read(getClass().getResource("img/oneill_west.png")));
		images.put("oneill_east", ImageIO.read(getClass().getResource("img/oneill_east.png")));
		images.put("oneill_south", ImageIO.read(getClass().getResource("img/oneill_south.png")));
		images.put("jaffa_north", ImageIO.read(getClass().getResource("img/jaffa_north.png")));
		images.put("jaffa_west", ImageIO.read(getClass().getResource("img/jaffa_west.png")));
		images.put("jaffa_east", ImageIO.read(getClass().getResource("img/jaffa_east.png")));
		images.put("jaffa_south", ImageIO.read(getClass().getResource("img/jaffa_south.png")));
		images.put("replicator", ImageIO.read(getClass().getResource("img/replicator.png")));
		
		// Sides double
		images.put("side_horizontal", ImageIO.read(getClass().getResource("img/side_horizontal.png")));
		images.put("side_vertical", ImageIO.read(getClass().getResource("img/side_vertical.png")));
		images.put("door_closed_horizontal", ImageIO.read(getClass().getResource("img/door_closed_horizontal.png")));
		images.put("door_closed_vertical", ImageIO.read(getClass().getResource("img/door_closed_vertical.png")));
		images.put("door_open_horizontal", ImageIO.read(getClass().getResource("img/door_open_horizontal.png")));
		images.put("door_open_vertical", ImageIO.read(getClass().getResource("img/door_open_vertical.png")));	
		
		// Sides single
		images.put("wall_horizontal", ImageIO.read(getClass().getResource("img/wall_horizontal.png")));
		images.put("wall_vertical", ImageIO.read(getClass().getResource("img/wall_vertical.png")));
		images.put("specwall_horizontal", ImageIO.read(getClass().getResource("img/specwall_horizontal.png")));
		images.put("specwall_vertical", ImageIO.read(getClass().getResource("img/specwall_vertical.png")));
		
		// Stargates and portals
		images.put("stargate_blue_closed_horizontal", ImageIO.read(getClass().getResource("img/stargate_blue_closed_horizontal.png")));
		images.put("stargate_blue_closed_vertical", ImageIO.read(getClass().getResource("img/stargate_blue_closed_vertical.png")));
		images.put("stargate_blue_open_horizontal", ImageIO.read(getClass().getResource("img/stargate_blue_open_horizontal.png")));
		images.put("stargate_blue_open_vertical", ImageIO.read(getClass().getResource("img/stargate_blue_open_vertical.png")));
		images.put("stargate_green_closed_horizontal", ImageIO.read(getClass().getResource("img/stargate_green_closed_horizontal.png")));
		images.put("stargate_green_closed_vertical", ImageIO.read(getClass().getResource("img/stargate_green_closed_vertical.png")));
		images.put("stargate_green_open_horizontal", ImageIO.read(getClass().getResource("img/stargate_green_open_horizontal.png")));
		images.put("stargate_green_open_vertical", ImageIO.read(getClass().getResource("img/stargate_green_open_vertical.png")));
		images.put("stargate_orange_closed_horizontal", ImageIO.read(getClass().getResource("img/stargate_orange_closed_horizontal.png")));
		images.put("stargate_orange_closed_vertical", ImageIO.read(getClass().getResource("img/stargate_orange_closed_vertical.png")));
		images.put("stargate_orange_open_horizontal", ImageIO.read(getClass().getResource("img/stargate_orange_open_horizontal.png")));
		images.put("stargate_orange_open_vertical", ImageIO.read(getClass().getResource("img/stargate_orange_open_vertical.png")));
		images.put("stargate_red_closed_horizontal", ImageIO.read(getClass().getResource("img/stargate_red_closed_horizontal.png")));
		images.put("stargate_red_closed_vertical", ImageIO.read(getClass().getResource("img/stargate_red_closed_vertical.png")));
		images.put("stargate_red_open_horizontal", ImageIO.read(getClass().getResource("img/stargate_red_open_horizontal.png")));
		images.put("stargate_red_open_vertical", ImageIO.read(getClass().getResource("img/stargate_red_open_vertical.png")));
	}
	
	/**
	 * Visszaadja az adott képet az ablak és a játékmezőhöz tartozó arány alapján csökkentve vagy növelve.
	 * @param imgKey A kép neve
	 * @return Az átméretezett kép
	 */
	public BufferedImage getImage(String imgKey) {
        //return images.get(imgKey);
        return resizeImage(images.get(imgKey), game.getWindowScale()*game.getMapScale());
	}
	
	/**
	 * Visszaadja az adott képet az ablakhoz tartozó arány alapján csökkentve vagy növelve.
	 * @param imgKey A kép neve
	 * @return Az átméretezett kép
	 */
	public BufferedImage getOnlyWindowScaledImage(String imgKey){
		return resizeImage(images.get(imgKey), game.getWindowScale());
	}
	
	/**
	 * Adott kép átméretezése.
	 */
    public BufferedImage resizeImage(BufferedImage originalImage, double scale){
    	int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
    	int width = (int) ((int)originalImage.getWidth()*scale);
    	int height = (int) ((int)originalImage.getHeight()*scale);
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		return resizedImage;
    }
    
    /**
     * Térkép átméretezés.
     */
	public int scaleDown(int original){
		return game.scaleMap(original);
	}
	
	/**
	 * Ablak átméretezés.
	 */
	public int scaleOnlyWindow(int original){
		return game.scaleWindow(original);
	}
}
