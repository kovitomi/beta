package graf;

/** 
 * Ezredes. Az ezredest képviseli.
 *
 */
public class Colonel extends Character {
	
	/**
	 * Konstruktor. A paramétereket a megfelelő attribútumoknak adjuk értékül.
	 * @param game A játék.
	 * @param selfWeight Az ezredes súlya.
	 * @param boxWeight A doboz súlya.
	 * @param zpm A megszerzett zpm-ek száma.
	 * @param maxzpm Az összegyűtendő zpm-ek száma.
	 * @param viewDirection Az irány, amerre néz.
	 */
	public Colonel(Game game, int selfWeight, int boxWeight, int zpm, int maxzpm, Direction viewDirection){
		this.game = game;
		this.selfWeight = selfWeight;
		this.boxWeight = boxWeight;
		this.zpm = zpm;
		this.maxzpm = maxzpm;
		this.viewDirection = viewDirection;
	}
	
	/**
	 * A kimeneti nyelv alapján kiírja az adatokat (típus: Colonel).
	 */
	public void showStateMovable() {
		//<típus> <súly> <doboz> <zpm> <maxzpm> <mező> <irány>
		System.out.println("Colonel " + selfWeight + " " + boxWeight + " " + zpm + " " + maxzpm + " " + location.getId() + " " + viewDirection);
	}
	
	/**
	 * Meghal. Szól a Game-nek (oneillLose()).
	 */
	public void die(Abyss abyss) {
		this.game.oneillLose();
	}
	
	/**
	 * Növeli a zpm-ek értékét, szól a Map-nek (manageZPM()), hogy felvette, true-t ad vissza (ezzel a mezőnek jelzi, hogy felvette).
	 * @return Igen/Nem.
	 */
	public boolean pickUpZpm() {
		zpm++;
		map.manageZPM();
		if(zpm == maxzpm) game.oneillVictory();
		return true;
	}
	
	/**
	 * A mező hívja, beállítja hogy Oneill az adott mezőn van-e.
	 * @param tile A mező.
	 * @param state Az adott mezőn van-e.
	 */
	public void setLocationBool(Tile tile, boolean state){
		tile.setOneill(state);
	}
	
}
