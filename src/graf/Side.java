package graf;
import java.util.ArrayList;
import java.util.LinkedHashMap;


/**
 * Oldal. Egy sima oldalt reprezentál (amikor két mező közvetlenül egymás mellett van), segít a mezőkön átmenni egyikről a másikra, ebből származik a többi oldal fajta.
 *
 */
public class Side {
	
	/**
	 * Azonosító.
	 */
	protected String id = "";
	
	/**
	 * A két irányban (amik ellentétesek egymással) tárolja a mezőket, amerre vannak.
	 */
	protected LinkedHashMap<Direction, Tile> tiles = new LinkedHashMap<Direction,Tile>();
	
	/**
	 * Atlehet-e rajta menni (true, ha igen, false, ha nem).
	 */
	protected Boolean permeable = true;
	
	/**
	 * CSAK a kiíráshoz használandó!
	 */
	protected String type = "";
	
	/**
	 * csillagkapu színe, vagy null ha nincs. Kiíráshoz.
	 */
	protected String stargateColor ="";
	/**
	 * A permeable-t true-ra állítja, beállítja az azonosítót.
	 * @param id Azonosító.
	 */
	public Side(String id){
		this.id = id;
		this.permeable = true;
		type = "open";
		stargateColor = "null";
	}
	
	/**
	 * Beállítja a mezőket (tiles), amiket határol.
	 * @param direction Melyik irányban.
	 * @param tile Melyik mező.
	 */
	public void setTiles(Direction direction, Tile tile) {
		tiles.put(direction, tile);
	}
	
	/**
	 * Visszaadja, hogy átlehet-e rajta menni (permeable-t). A paraméter csillagkapu esetén szükséges, máshol nem használjuk.
	 * @param movable A mozgatható objektum.
	 * @return Igen/Nem.
	 */
	public boolean canMove(Movable movable) {
		return permeable;
	}

	/** 
	 * Visszaadja, hogy átlehet-e rajta lőni, true-t.
	 * @return Igen/Nem.
	 */
	public boolean canShootThrough () {
		return permeable;
	}

	/** 
	 * Adott irányban található mezőt adja vissza, null-t ha abban az irányban nincs.
	 * @param direction Az irany.
	 * @return A mezo.
	 */
	public Tile getTile(Direction direction) {
		return tiles.get(direction);
	}

	/**
	 * Lehet-e rá csillagkaput rakni: false-t ad vissza.
	 * @return Igen/Nem.
	 */
	public Boolean isStargatePlacable() {
		return false;
	}
	
	/**
	 * A kimeneti nyelvnek megfelelően kiírja az adatokat.
	 */
	public void showStateSide() {
		//<azonosító> <típus> <átjárható> <átlőhető> <mező1> <mező2> <csillagkapu>
		ArrayList<String> tile = new ArrayList<String>();
		if(tiles.get(Direction.North) != null) tile.add(tiles.get(Direction.North).getId());
		if(tiles.get(Direction.West) != null) tile.add(tiles.get(Direction.West).getId());
		if(tiles.get(Direction.East) != null) tile.add(tiles.get(Direction.East).getId());
		if(tiles.get(Direction.South) != null) tile.add(tiles.get(Direction.South).getId());
		if(tile.size() < 2) {
			tile.add("null");
			tile.add("null");
		}
		System.out.println(id + " " + type + " " + canMove(null) + " " + canShootThrough() + " " + tile.get(0) + " " + tile.get(1) + " " + stargateColor);
	}
	
	/**
	 * Visszaadja az azonosítót.
	 * @return Az azonosító.
	 */
	public String getId() {
		return id;
	}
	
}
