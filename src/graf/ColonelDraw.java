package graf;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

/** 
 * Az ezredes kirajzolását végzi.
 *
 */
public class ColonelDraw extends MovableDraw {
	
	/** 
	 * Referencia az ezredesre.
	 */
	private Colonel oneill;
	
	/** 
	 * Konstruktor
	 * @param oneill Az ezredes.
	 */
	public ColonelDraw(Colonel oneill) {
	    super(oneill);
	    this.oneill = oneill;
	}
	
	/** 
	 * Kirajzolja az ezredest.
	 */
	public void draw(Graphics g) {
		String imageName = "oneill_" + oneill.getDirection().toString().toLowerCase();
		pos = getPosition();
		if(pos != null) {
		    g.drawImage(imageHandler.getImage(imageName), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		}
		String boxWeightString = "-";
		if(oneill.boxWeight > 0) boxWeightString = oneill.boxWeight + " kg";
		
		// Információs sávon az adatok
		Dimension windowSize = oneill.game.getWindow().getSize();
		int infoPaneX = (int)windowSize.getWidth()-imageHandler.scaleOnlyWindow(200);
		g.setColor(java.awt.Color.white);
		g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(36)));
		g.drawString("Ezredes", infoPaneX+imageHandler.scaleOnlyWindow(30), imageHandler.scaleOnlyWindow(180));
		g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(25)));
		g.drawString("Súly: " + oneill.getWeight() + " kg", infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(210));
		g.drawString("Doboz: " + boxWeightString, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(240));
		g.drawString("Zpm: " + oneill.zpm, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(270));
		g.drawString("Kell: " + oneill.maxzpm, infoPaneX+imageHandler.scaleOnlyWindow(20), imageHandler.scaleOnlyWindow(300));
	}
}
