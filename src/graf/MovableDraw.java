package graf;



/**
 * Azon objektumok őse, akik változtathatják a pozíciójukat (Colonel, Jaffa, Replicator).
 * 
 */
public abstract class MovableDraw extends Drawable {
	
    /**
	 * Pozívió lekéréséhez kell, ugyanaz, mint ami a leszármazottnál van.
	 */
    private Movable movable;
	
	/**
	 * Game-re referencia.
	 */
	private Game game;
	
	/**
	 * Konstruktor.
	 */
	public MovableDraw(Movable movable) {
		this.movable = movable;
	}
	
	/**
	 * Beállítja a Game-et.
	 * @param game A Game.
	 */
	public void setGame(Game game) {
	    this.game = game;
	}
	
	/**
	 * A képernyőn a pozíciót adja vissza, elsősorban az örökölt osztályoknak kell: 
	 * Először meghatározza, hogy melyik mezőn áll (getLocation()), 
	 * majd annak azonosítója alapján (getId()) a Game-től lekérdezni, 
	 * hogy mi a pozíciója a mezőnek (getPosition()), ezt pedig visszaadja.
	 */
	public Position getPosition() {
	    return game.getPosition(movable.getLocation().getId());
	}
}
