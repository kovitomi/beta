package graf;


/**
 * A replikátor. Mozog a pályán véletlenszerűen, ha szakadékba esik átalakítja mezővé (és ha lelövik megsemmisül).
 *
 */
public class Replicator extends Movable {
	
	/**
	 * Replikátor él-e (vagy lelőtték, illetve átalakított egy szakadékot).
	 */
	protected Boolean alive = true;
	
	/**
	 * Beállítja a nézési irányt.
	 * @param game A játék kezelő.
	 * @param direction Az irány
	 */
	public Replicator(Game game, Direction viewDirection){
		this.game = game;
		this.viewDirection = viewDirection;
	}
	
	/**
	 * A kapott szakadékot átalakítjuk mezővé (convert(), lásd Abyss) és szólunk a játéknak (Game) és pályának (Map), hogy meghalt.
	 * @param abyss Az abyss, amit konvertálni kell.
	 */
	public void die(Abyss abyss){
		abyss.convert();
		map.replicatorDied();
		game.replicatorDied();
		alive = false;
	}
	
	/**
	 * Nem veszi fel a ZPM-et: false-t ad vissza.
	 * @return Nem.
	 */
	public boolean pickUpZpm(){
		return false;
	}
	
	/**
	 * Lelőtték a replkátort ezt kell hívni.
	 */
	public void shotDown() {
		alive = false;
		game.replicatorDied();
		map.replicatorDied();
	}
	/**
	 * Kimeneti nyelvnek megfelelően kiírja a replikátor adatait.
	 */
	public void showStateMovable(){
		//<típus> <állapot> <mező> <irány>
		System.out.println("Replicator " + alive + " " + location.getId() + " " + viewDirection);
	}
	
	/**
	 * A mező hívja, beállítja hogy a replikátor az adott mezőn van-e.
	 * @param tile A mező.
	 * @param state Az adott mezőn van-e.
	 */
	public void setLocationBool(Tile tile, boolean state){
		tile.setReplicator(state);
	}
	
	/**
	 * Replikátor él-e.
	 * @return Él-e.
	 */
	public Boolean isAlive() {
	    return alive;
	}
}
