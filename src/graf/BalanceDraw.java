package graf;

import java.awt.Font;
import java.awt.Graphics;

/** 
 * Egy mérleg kirajzolását végzi.
 *
 */
public class BalanceDraw extends Drawable {
	
	/** 
	 * Referencia a kirajzolandó mérlegre.
	 */
	private Balance balance;
	
	/** 
	 * Konstruktor.
	 * @param balance A mérleg.
	 */
	public BalanceDraw(Balance balance) {
		this.balance = balance;
	}
	
	/**
	 * Kirajzolja a mérleget.
	 */
	public void draw(Graphics g) {
		g.drawImage(imageHandler.getImage("balance"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		g.setColor(java.awt.Color.black);
		g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(12)));
		
		g.drawString("/" + balance.getWeightLimit(), imageHandler.scaleDown(pos.getX()+31), imageHandler.scaleDown(pos.getY()+104));
	}
}
