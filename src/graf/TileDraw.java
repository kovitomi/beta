package graf;

import java.awt.Font;
import java.awt.Graphics;

/**
 * Egy mező kirajzolását végzi.
 * 
 */
public class TileDraw extends Drawable {
	
	/**
	 * Referencia a kirajzolandó mezőre.
	 */
	protected Tile tile;
	
	/**
	 * Mérleg kirajzolásához kell, a rajta levő mérleg kirajzoló objektumára referencia.
	 */
	private BalanceDraw balance;
	
	/**
	 * Konstruktor.
	 * @param tile A mező.
	 */
	public TileDraw(Tile tile) {
		this.tile = tile;
	}
	
	/**
	 * A mezőn levő mérleg beállítása.
	 * @param balance A mérleg rajzoló objektuma.
	 */
	public void setBalance(BalanceDraw balance) {
		this.balance = balance;
	}
	
	/**
	 * Kirajzolja a mezőt, a dobozt rajta, szól a mérlegnek, hogy rajzolja ki magát (ha van rajta).
	 */
	public void draw(Graphics g) {
		g.drawImage(imageHandler.getImage("tile"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		g.setColor(java.awt.Color.black);
		g.setFont(new Font("default", Font.BOLD, imageHandler.scaleOnlyWindow(12)));
		
		if(tile.hasBox()){
			g.drawImage(imageHandler.getImage("box"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
			if(tile.topBoxWeight() > 9){
				g.drawString(tile.topBoxWeight() + "", imageHandler.scaleDown(pos.getX()+26), imageHandler.scaleDown(pos.getY()+88));
			}else{
				g.drawString(tile.topBoxWeight() + "", imageHandler.scaleDown(pos.getX()+30), imageHandler.scaleDown(pos.getY()+88));				
			}
		}
		
		if(tile.getZpm()) g.drawImage(imageHandler.getImage("zpm"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);

		if(balance != null){
			int totalWeight = tile.boxFullWeight() + tile.getCharacterWeight();
			if(totalWeight > 9){
				g.drawString(totalWeight + "", imageHandler.scaleDown(pos.getX()+18), imageHandler.scaleDown(pos.getY()+104));
			}else{
				g.drawString(totalWeight + "", imageHandler.scaleDown(pos.getX()+20), imageHandler.scaleDown(pos.getY()+104));
			}

			balance.draw(g);
		}
	}
}
