package graf;

/** 
 * Fal. Nem enged át magán semmit.
 *
 */
public class Wall extends Side {
	
	/**
	 * Meghívja az ős konstruktorát, permeable-t false-ba rakja.
	 */
	public Wall(String id){
		super(id);
		this.permeable = false;
		type = "wall";
	}
	
	/**
	 * Felüldefiniájuk az örököltet: false-t ad vissza.
	 * @return Nem.
	 */
	@Override
	public boolean canShootThrough(){
		return false;
	}
	
}
