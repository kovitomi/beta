package graf;

import java.awt.Graphics;

/** 
 * Egy szakadék kirajzolását végzi.
 *
 */
public class AbyssDraw extends TileDraw {
	
	/** 
	 * Referencia a kirajzolandó szakadékra.
	 */
	private Abyss abyss;
	
	/** 
	 * Konstruktor.
	 * @param abyss A szakadék.
	 */
	public AbyssDraw(Abyss abyss) {
        super(null);
        this.abyss = abyss;
	    tile = abyss;
	}
	
	/** 
	 * Kirajzolja az adott szakadékot. Ha konvertált, mezőt rajzol ki.
	 */
	public void draw(Graphics g) {
		if(abyss.getConverted()){
			super.draw(g);
		}else{
			g.drawImage(imageHandler.getImage("abyss"), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		}
	}
}
