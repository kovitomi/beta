package graf;
import java.util.ArrayList;
import java.util.LinkedHashMap;


/** 
 * Féregjárat. A féregjáratok, a rajtuk történő áthaladás, a csillagkapuk létrehozásának kezelése.
 *
 */
public class Portal {
	
	/**
	 * Minden színhez letároljuk, hogy nyitva van-e a hozzá tartozó féregjárat.
	 */
	private LinkedHashMap<Color, Boolean> isOpen = new LinkedHashMap<Color, Boolean>();
	/**
	 * Stargate-ekre referencia (színükkel lehet indexelni).
	 */
	private LinkedHashMap<Color, Stargate> stargates = new LinkedHashMap<Color, Stargate>();
	/**
	 * Speciális falakra referencia, amiken a csillagkapuk vannak (a csillagkapuk színével lehet indexelni).
	 */
	private LinkedHashMap<Color, SpecialWall> specialWalls = new LinkedHashMap<Color, SpecialWall>();

	/**
	 * Mindent töröl és új, üres LinkedHashMap-eket vesz fel.
	 */
	public Portal() {
		isOpen = new LinkedHashMap<Color, Boolean>();
		isOpen.put(Color.Blue, false);
		isOpen.put(Color.Orange, false);
		isOpen.put(Color.Red, false);
		isOpen.put(Color.Green, false);
		stargates = new LinkedHashMap<Color, Stargate>();
		specialWalls = new LinkedHashMap<Color, SpecialWall>();
	}
	
	/**
	 * Megkapja (a Map-től), hogy melyik oldalra (ami lehet SpecialWall (speciális fal), Wall (fal), zárt Door (ajtó)), milyen színnel lőttek.
	 * Az odlaltól megkérezi, hogy lehet-e rá csillagkaput helyezni (isStargatePlaceable()), ha igen (csak speciális falra lehet, rajta levő esetleges
	 * csillagkapuktól függetlenül true-t ad, a többi false-t) lekéri tőle a csillagkaput, ha nincs rajta null-t kap.
	 * Ha van rajta: és ugyanolyan színű, nem csinál semmit. Ha különböző színű megszünteti és felrakja a most lőttet, a most lőttből ha volt eredetileg máshol
	 * leveszi. Ha nincs rajta: megszünteti az eredetileg létező lőtt színűt (ha volt), majd felrakja a mostanit. Az átjárhatóságot ennek megfelelően változtatja.
	 * Ha az odlalra nem lehet csillagkaput helyezni, nem csinálunk semmit. Ha létrehoztunk csillagkaput, vagy módosítottunk már létezőt azt visszaadjuk,
	 * ha nem akkor null-t adunk vissza.
	 * @param side Ahova a csillagkapunak kerülnie kell. 
	 * @param color A csillagkapu szine.
	 * @return A létrejött csillagkapu, null ha nem tudott létrehozni.
	 */
	public Stargate stargateShot(Side side, Color color) {
		if(side.isStargatePlacable()) {
			SpecialWall theSideWall = (SpecialWall)side;
			if(theSideWall.isStargate()) {
				Stargate oldStargate = theSideWall.getStargate();
				if(!oldStargate.getColor().equals(color)) {
					Stargate newStargate = new Stargate(color, this);
					
					//Eredeti színűhöz tartozó féregjáraton nem lehet átmenni:
					isOpen.put(oldStargate.getColor(), false);
					isOpen.put(oldStargate.getColor().getOther(), false);
					
					//Az eredetileg a falon levő csillagkaput töröljük:
					specialWalls.remove(oldStargate.getColor());
					stargates.remove(oldStargate.getColor());
					
					SpecialWall spec = specialWalls.get(color);
					if(spec != null) {
					    spec.setStargate(null);
					}
					
					//Beállítjuk az újat a speciális falon, ezzel felülírjuk a megegyező színű csillagkaput:
					specialWalls.put(color, theSideWall);
					theSideWall.setStargate(newStargate);
					
					//Féregjáraton is beállítjuk:
					stargates.put(color, newStargate);
					
					//Megnézzük, hogy átlehet-e rajta menni:
					if(stargates.get(color.getOther()) != null) {
						isOpen.put(color, true);
						isOpen.put(color.getOther(), true);
					}
					return newStargate;
				} else {
					return null;
				}
			} else {
				Stargate newStargate = new Stargate(color, this);
				//Töröljük a már létező ilyen szinűt:
				if(specialWalls.get(color) != null){
					specialWalls.get(color).setStargate(null);
					specialWalls.remove(color);
				}
				//Beállítjuk az újat:
				stargates.put(color, newStargate);
				specialWalls.put(color, theSideWall);
				theSideWall.setStargate(newStargate);
				
				//Megnézzük, hogy átlehet-e rajta menni:
				if(stargates.get(color.getOther()) != null) {
					isOpen.put(color, true);
					isOpen.put(color.getOther(), true);
				}
				return newStargate;
			}
		}
	    return null;
	}
	
	/**
	 * isOpen indexelésével (a kapott színnel) visszaadja, hogy átlehet-e menni a megfelelő féregjáraton (true, ha igen, false, ha nem).
	 * @return Igen/Nem.
	 */
	public Boolean isPermeable(Color color) {
		return isOpen.get(color);
	}

	/**
	 * Az adott színű csillagkapu nézési irányát határozza meg és adja vissza, null-t ha nem létezik (ilyen esetnek nem szabad előfordulnia).
	 * stargates-t a színnel indexelve megkapja a csillagkaput, tőle lekérdezi a nézési irányát (getDirection(), lásd Stargate) és azt visszaadja.
	 * @param color A szín.
	 * @return Az irány.
	 */
	public Direction getDirection(Color color) {
		if(stargates.get(color) != null) return stargates.get(color).getDirection();
		else return null;
	}

	/**
	 * specialWalls kapott színnel való indexelésével megkapja a speciális falat, amin a csillagkapu van, ettől lekérdezi a mezőt (getRealTile(),
	 * csillagkapu nézési irányában levőt, ez a LinkedHashMap-es tárolás miatt szükséges) amit határol és azt visszaadja.
	 * @param color A szín.
	 * @return A mező.
	 */
	public Tile getTile(Color color) {
		if(specialWalls.get(color) != null) return specialWalls.get(color).getRealTile(stargates.get(color).getDirection());
		else return null;
	}
	
	/**
	 * A kimeneti nyelvnek megfelelően kiírja az adatokat (a megfelelő lekérdezésekkel, isOpen, stargates, specialWalls indexelésével,
	 * adatainak lekérdezésével (ha léteznek), getRealTie(), lásd SpecialWall).
	 */
	public void showStatePortal() {
		//<szín> <mező> <oldal> <irány> <állapot>
		ArrayList<Color> colors = new ArrayList<Color>();
		colors.add(Color.Blue);
		colors.add(Color.Orange);
		colors.add(Color.Red);
		colors.add(Color.Green);
		for(int n=0;n<colors.size();n++) {
			String tile = "null", side = "null", direction = "null", state = "false";
			if(getTile(colors.get(n)) != null) tile = getTile(colors.get(n)).getId();
			if(specialWalls.get(colors.get(n)) != null) side = specialWalls.get(colors.get(n)).getId();
			if(getDirection(colors.get(n)) != null) direction = getDirection(colors.get(n)).toString();
			if(stargates.get(colors.get(n)) != null) state = isOpen.get(colors.get(n)).toString();
			System.out.println(colors.get(n).toString() + " " + tile + " " + side + " " + direction + " " + state);
		}
		
	}
	
	
}
