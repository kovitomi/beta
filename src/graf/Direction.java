package graf;

/**
 * Iranyok. Ez egy enum, az irámyokért (Négy égtáj: North, South, East, West) és kezelésükért felelős.
 *
 */
public enum Direction {
	North, South, East, West; 
	
	/**
	 * Visszaadja az ellenkező irányt (arra nézve, amelyiken meghívjuk).
	 * @return Az irány.
	 */
	public Direction getOpposite() {
		//North -> South
		if(this == Direction.North)
			return Direction.South;
		//East -> West
		else if(this == Direction.East)
			return Direction.West;
		//South -> North
		else if(this == Direction.South)
			return Direction.North;
		//West -> East
		else
			return Direction.East;
	}
	
	/**
	 * Egy irányon meghívjuk, és az ahhoz képest (tehát ha arra néznénk) 90°-kal balra elforgatott irányt (90°-kal balra nézünk) adja vissza.
	 * @return Az irány.
	 */
	public Direction getLeft() {
		//North -> West
		if(this == Direction.North)
			return Direction.West;
		//East -> North
		else if(this == Direction.East)
			return Direction.North;
		//South -> East
		else if(this == Direction.South)
			return Direction.East;
		//West -> South
		else
			return Direction.South;
	}
	
	/**
	 * Ugyanaz, mint a getLeft(), csak éppen 90°-kal jobbra.
	 * @return Az irány.
	 */
	public Direction getRight() {
		//North -> East
		if(this == Direction.North)
			return Direction.East;
		//East -> South
		else if(this == Direction.East)
			return Direction.South;
		//South -> West
		else if(this == Direction.South)
			return Direction.West;
		//West -> North
		else
			return Direction.North;
	}
	
}
