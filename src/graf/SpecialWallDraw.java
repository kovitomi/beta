package graf;

import java.awt.Graphics;

/**
 * Egy speciális fal kirajzolását végzi.
 * 
 */
public class SpecialWallDraw extends SideDraw {
	
	/**
	 * Referencia a kirajzolandó speciális falra.
	 */
	private SpecialWall spec;
	
	/**
	 * Konstruktor.
	 */
	public SpecialWallDraw(SpecialWall specialWall) {
        super(null);
        this.spec = specialWall;
	}
	
	/**
	 * Ha van rajta csillagkapu (isStargate(), getStargate(), lásd SpecialWall), 
	 * lekérdezi a színét (getColor(), lásd Stagate) és annak alapján rajzol, 
	 * ha nincs akkor simán kirajzolja magát.
	 */
	public void draw(Graphics g) {
		String imageNamePermeable;
		if(spec.canMove(null)){
			imageNamePermeable = "open";
		}else{
			imageNamePermeable = "closed";
		}
		String imageNameDirection;
		if(isVertical){
			imageNameDirection = "vertical";
		}else{
			imageNameDirection = "horizontal";
		}
		String imageNameSpec;
		
		imageNameSpec = "specwall_" + imageNameDirection;
		
		g.drawImage(imageHandler.getImage(imageNameSpec), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		
		if(spec.isStargate()){
			imageNameSpec = "stargate_" + spec.getStargate().getColor().toString().toLowerCase() + "_" + imageNamePermeable + "_" + imageNameDirection;
			g.drawImage(imageHandler.getImage(imageNameSpec), imageHandler.scaleDown(pos.getX()), imageHandler.scaleDown(pos.getY()), null);
		}
		
		
	}
}
