package graf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

/** 
 * Pálya. Ismeri a pályát, a karaktereket a pályát, a replikátort a pályán. Lövés útvonalának megtalálásáért felelős.
 *
 */
public class Map {

	/**
	 * Az ezredesre referencia.
	 */
	private Colonel oneill = null;
	
	/**
	 * A jaffára referencia.
	 */
	private Jaffa jaffa = null;
	
	/**
	 * Az éppen élő replikátorra referencia.
	 */
	private Replicator replicator = null;
	
	/**
	 * A féregjáratra (Potral) referencia. A Portal akkor is létezik, ha tényleges féregjárat nincs csillagkapuk között, mert ő kezeli az átjárást a csillagkapukon (lásd Portal).
	 */
	private Portal portal = null;
	
	/**
	 * Játék kezelőre referencia.
	 */
	private Game game;
	/**
	 * A pálya összes mezeje.
	 */
	private LinkedHashMap<String, Tile> tiles = null;
	
	/**
	 * A pálya összes oldala.
	 */
	private LinkedHashMap<String, Side> sides = null;
	
	/**
	 * A pálya összes mérlege.
	 */
	private LinkedHashMap<String, Balance> balance = null;
	
	/**
	 * True, ha teszt módban, false, ha élesben fut a program. Véletlenszerűségek miatt fontos, azok determinisztikusak lesznek:
	 * a replikátort lehet irányítani, a generált ZPM helyét meglehet mondani.
	 */
	private boolean isTest = false;
	
	/**
	 * Számolja, hogy hányszor szóltak neki (Map-nek), hogy felvettek egy ZPM-et. Csak kettőig számol, ekkor resetel.
	 */
	private int zpmGenCount = 0;
	
	/**
	 * A paramétereket értelemszerűen értékül adjuk a megfelelő attribútumnak.
	 * @param oneill Referencia az ezredesre.
	 * @param jaffa Referencia a jaffára.
	 * @param portal Referencia az féregjáratra.
	 * @param game Referencia a játékra.
	 * @param tiles Referencia a mezőkre.
	 * @param sides Referencia az oldalakra.
	 * @param balance Referencia az mérlegekre.
	 * @param isTest Valós, vagy teszt mód.
	 */
	public Map(Colonel oneill, Jaffa jaffa, Replicator replicator, Portal portal, Game game, LinkedHashMap<String, Tile> tiles, LinkedHashMap<String, Side> sides, LinkedHashMap<String, Balance> balance, boolean isTest) {
		this.oneill = oneill;
		this.jaffa = jaffa;
		this.replicator = replicator;
		this.portal = portal;
		this.game = game;
		this.tiles = tiles;
		this.sides = sides;
		this.balance = balance;
		this.isTest = isTest;
	}
	
	/**
	 * Karakter hívja rajta, aki lőtt, megkapja az induló mezőt (amin a karakter áll), az irányt, amerre lő, a színt, amilyen lövedéket lő.
	 * A mezőből kiindulva a következőket csinálja egy ciklusban: lekéri az adott irányban a mezőtől az oldalt,
	 * ha átlehet lőni rajta (Side-on canShootThrough() ha true-t ad vissza) lekéri az adott irányban a következő mezőt a Side-tól (getTile()),
	 * majd vissza ugrik a ciklus elejére és a kapott mezőből kiindulva folytatja. Addig fut a ciklus, amíg talál valamit, 
	 * amin nem tud átlőni (Ez valamilyen Side lehet: SpecialWall (speciális fal), Wall (fal) vagy zárt Door (ajtó)) és ezt a Side-ot,
	 * illetve az adott színt a Portal-nak a stargateShot()-ban paraméterként átadja (lásd Portal), ami visszaad egy csillagkaput (Stargate),
	 * ha létre tudott hozni, ebben az esetben beállítjuk a nézési irányát (setDirection(), lásd Stargate), ha nem sikerült null-t.
	 * Ha a lövedék útjának keresése közben (a ciklusban) talál egy mezőn egy replikátort (minden mezőn ellenőrizni kell, hogy van-e),
	 * azt megsemmisíti és null-lal visszatér.
	 * @param tile A kiinduló mező.
	 * @param direction Az irány, amerre lő.
	 * @param color A lövedék színe.
	 */
	public void shoot(Tile tile, Direction direction, Color color) {
		Side tileSide = tile.tileSide(direction);
		Boolean doNotCreate = false;
		tile.showStateTile();
		
		//Megkeresi egyenes volnalban a legközelebbi akadályt:
		while(tileSide.canShootThrough()) {
			if(tileSide.getTile(direction).getReplicator()) {
				doNotCreate = true;
				replicator.shotDown();
				tileSide.showStateSide();
				replicator.showStateMovable();
				break;
			}
			tileSide.showStateSide();
			tileSide.getTile(direction).showStateTile();
			tileSide = tileSide.getTile(direction).tileSide(direction);
		}
		
		//Szólunk a Portal-nak, hogy hova kell csillagkaput tenni:
		Stargate created = null;
		if(!doNotCreate) {
			created = portal.stargateShot(tileSide, color);
		}
		
		//Ha sikerült csillagkaput létrehozni, beállítjuk azt, hogy merre néz:
		if(created!=null) {
			created.setDirection(direction.getOpposite());
		}
		
		if(!doNotCreate) {
			tileSide.showStateSide();
			portal.showStatePortal();
		}
	}
	
	/**
	 * Ezt a függvényt hívja az a karakter, akinek szólnia kell, ha felvett egy ZPM-et. Növeli a számlálót (zpmGenCount, 0-tól kezdve), ha 2-t eléri,
	 * reseteli (0-ba állítja) és generál véletlenszerű helyre (vagy teszt esetén előre meghatározott helyre) egy új ZPM-et. Lehetőleg olyan helyre,
	 * ami sima mező vagy konvertált szakadék, nincs rajta ZPM és karakter, de ez nem minden esetben lehet (ha pl. túl kicsi a pálya és nincs ilyen hely),
	 * ekkor mindegy, hova rakjuk (teljesen véletlenszerű).
	 */
	public void manageZPM(){
		zpmGenCount++;
		if(zpmGenCount>=2) {
			zpmGenCount=0;
			Tile newZpmLocation = null;
			if(isTest) {
				if(tiles.get("T5") != null) {
					newZpmLocation = tiles.get("T5");
				}
			} else {
				String oneillPlace = oneill.getLocation().getId();
				String jaffaPlace = jaffa.getLocation().getId();
				ArrayList<String> emptyPlaces = new ArrayList<String>();
				
				//Minden mezőre megnézzük, van-e benne ZPM, ha nincs eltároljuk
				for(Entry<String, Tile> entry : tiles.entrySet()) {
					if(!entry.getValue().getZpm()) {
						emptyPlaces.add(entry.getKey());
					}
				}
				emptyPlaces.remove(oneillPlace);
				emptyPlaces.remove(jaffaPlace);
				int tileCount = emptyPlaces.size();
				
				//Azok a mezők közül, amiken nincs se karakter se zpm, egyre generálunk egy zpm-et:
				if(tileCount>0) {
					int randomIdx = ThreadLocalRandom.current().nextInt(tileCount);
					if(tiles.get(emptyPlaces.get(randomIdx))!=null) {
						newZpmLocation = tiles.get(emptyPlaces.get(randomIdx));
					}
				//Ha nincs olyan mező, amire ez nem teljesül, rárakjuk valamelyik karakter mezőjére:
				} else {
					emptyPlaces.add(oneillPlace);
					emptyPlaces.add(jaffaPlace);
					int randomIdx = ThreadLocalRandom.current().nextInt(emptyPlaces.size());
					if(tiles.get(emptyPlaces.get(randomIdx))!=null) {
						newZpmLocation = tiles.get(emptyPlaces.get(randomIdx));
					}
				}
			}
			newZpmLocation.setZpm(true);
			newZpmLocation.showStateTile();
		}
	}
	
	/**
	 * Végig iterál a mezőkön (tiles), oldalakon(sides) és kiíratja az adatait (showStateTile(), lásd Tile, showStateSide(), lásd Side),
	 * az ezredesnek (oneill), a jaffának (jaffa), replikátornak (replicator), a féregjáratnak (portal) is kiíratja az adatait
	 * (showStateCharacter(), lásd Character, Colonel, Jaffa, showStateReplicator(), lásd Replicator, showStatePortal(), lásd Portal).
	 */
	public void showStateAll(){
		for (Entry<String, Tile> entry : tiles.entrySet()) {
			entry.getValue().showStateTile();
		}
		for (Entry<String, Side> entry : sides.entrySet()) {
			entry.getValue().showStateSide();
		}
		oneill.showStateMovable();
		jaffa.showStateMovable();
		replicator.showStateMovable();
		portal.showStatePortal();
	}
	
	/**
	 * Ezt kell hívni, ha meghal a replikátor.
	 */
	public void replicatorDied() {
		replicator.getLocation().setReplicator(false);
	}
}
