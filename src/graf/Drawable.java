package graf;

import java.awt.Graphics;



/** 
 * Mindennek az őse, akit kilehet rajzolni, a pozíciót tárolja, rajzoló függvényt deklarál.
 *
 */
public abstract class Drawable {
	
	/** 
	 * Tárolja a képernyőn a pozíciót.
	 */
	protected Position pos = new Position();
	
	/**
	 * Vertikális-e. A 64x128 vagy 128x64 pálya elemeknek kell (elsősorban SpecialWall-nak)
	 */
	protected boolean isVertical;
	
	/** 
	 * A képkezelőre referencia.
	 */
	protected ImageHandler imageHandler;
	
	/** 
	 * Kirajzol egy objektumot, minden leszármazott osztályban felül kell definiálni.
	 */
	public abstract void draw(Graphics g);
        
	/**
	 * Beállítja az Imagehandlert.
	 * @param imageHandler Az ImageHandler.
	 */
	public void setImageHandler(ImageHandler imageHandler) {
	    this.imageHandler = imageHandler;
	}
    
	/**
	 * Beállítja a pozícióját pixelekben kifejezve.
	 * @param x Az x koordináta.
	 * @param y Az y koordináta.
	 */
	public void setPosition(int x, int y) {
	    pos.setX(x);
	    pos.setY(y);
	}
	
	/**
	* Beállítja, hogy vertikális-e.
	* @param isVertical Igen/nem.
	*/
	public void setVertical(Boolean isVertical) {
	    this.isVertical = isVertical;
	}
}
