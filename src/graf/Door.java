package graf;

/** 
 * Ajto. Két mező között átjárást biztosít, ha nyitva van, nem lehet átmenni, ha zárva van (úgy viselkedik, mint a fal). Persze akkor, ha az ajtó választja el a két mezőt.
 *
 */
public class Door extends Side {
	
	/**
	 * Konstruktor.
	 * @param id Az azonosító.
	 */
	public Door(String id) {
		super(id);
		permeable = false;
		type = "door";
	}

	/**
	 * Ha a paraméter igaz (true) az ajtót nyitott állapotba helyezi, ha false zártba (permeable). Ezt a függvényt hívja a mérleg (Balance).
	 * @param open Nyitva/Zarva.
	 */
	public void setOpen(Boolean open) {
		permeable = open;
	}
	
}
