@echo off
if not exist Abyss.java goto JAVA_FILE_MISSING
if not exist Balance.java goto JAVA_FILE_MISSING
if not exist Colonel.java goto JAVA_FILE_MISSING
if not exist Color.java goto JAVA_FILE_MISSING
if not exist Direction.java goto JAVA_FILE_MISSING
if not exist Door.java goto JAVA_FILE_MISSING
if not exist Game.java goto JAVA_FILE_MISSING
if not exist Map.java goto JAVA_FILE_MISSING
if not exist Portal.java goto JAVA_FILE_MISSING
if not exist Side.java goto JAVA_FILE_MISSING
if not exist SpecialWall.java goto JAVA_FILE_MISSING
if not exist Stargate.java goto JAVA_FILE_MISSING
if not exist Tile.java goto JAVA_FILE_MISSING
if not exist Wall.java goto JAVA_FILE_MISSING

::C:\Progra~1\Java\jdk1.7.0_79\bin\javac.exe *.java
javac *.java

mkdir szkeleton
move *.class szkeleton
cls
java szkeleton.Game
rd /s/q szkeleton

goto END

:JAVA_FILE_MISSING
echo One or more .java files are missing!
pause

:END