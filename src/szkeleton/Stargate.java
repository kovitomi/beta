package szkeleton;

/** 
 * Csillagkapu.
 *
 */
public class Stargate extends Side {
	
	/** 
	 * A csillagkapu szine.
	 */
	public Color color = null;
	/** 
	 * A másik szin.
	 */
	public Color otherColor = null;
	/** 
	 * A feregjarat.
	 */
	public Portal portal = null;
	/** 
	 * Az irany amerre a csillagkapu nez.
	 */
	public Direction viewDirection = null;
	/** 
	 * Az ezredes.
	 */
	Colonel oniel = null;
	
	/**
	 * Csillagkapu konstruktor.
	 * @param newColor Csillagkapu szine.
	 * @param newPortal A feregjarat.
	 * @param newOniel Az ezredes.
	 */
	public Stargate(Color newColor, Portal newPortal, Colonel newOniel) {
		color = newColor;
		otherColor = color.getOther();
		portal = newPortal;
		oniel = newOniel;
	}
	
	/**
	 * A csillagkapu szinet adja vissza.
	 * @return A szin.
	 */
	public Color getColor() {
		System.out.printf("Stargate.getColor() (color=%s)\n", color); 
		return color;
	}
	
	/**
	 * Visszaadja az iranyt, amerre nez a csillagkapu.
	 * @return Az irany.
	 */
	public Direction getDirection() {
		System.out.println("Stargate.getDirection()"); 
		return viewDirection;
	}
	
	/**
	 * Beallitja, hogy milyen iranyba nez a csillagkapu.
	 * @param viewDirection Az irany.
	 */
	public void setDirection(Direction viewDirection) {
		System.out.printf("Stargate.setDirection() (direction=%s)\n", viewDirection); 
		this.viewDirection = viewDirection;
	}
	
	/**
	 * At lehet-e menni a feregjaraton. Ha igen arra forditja az ezredest, amerre a masik oldali csaillagkapu nez.
	 * @return Igen/Nem.
	 */
	@Override
	public Boolean canMove() {
		System.out.println("Stargate.canMove()");
		if(portal.isPermeable()) {
			oniel.look(portal.getDirection(color.getOther()));
			return true;
		} else return false;
	}
	
	/**
	 * A feregjarat tul oldalan levo mezot adja vissza.
	 * @param direction Az irany.
	 * @return A mezo.
	 */
	@Override
	public Tile getTile(Direction direction) {
		System.out.println("Stargate.getTile()");
		return portal.getTile(color.getOther());
	}
	
}
