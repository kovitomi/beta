package szkeleton;

/** 
 * Szakadek.
 *
 */
public class Abyss extends Tile {
	
	/**
	 * Az ezredes megerkezik a szakadekba.
	 * @param oniel Az ezredes.
	 */
	@Override
	public void arrive(Colonel oniel) {
		System.out.println("Abyss.arrive()");
		oniel.die();
	}
	
	/**
	 * A szakadekba esik-e a doboz.
	 * @param is Igen/Nem.
	 */
	public void setBox(Boolean is) {
		System.out.println("Abyss.setBox()");
	}
	
}
