package szkeleton;

/** 
 * Specialis fal.
 *
 */
public class SpecialWall extends Wall {

	/** 
	 * A falon talalhato csillagkapu.
	 */
	public Stargate stargate = null;
	/** 
	 * Van-e rajta csillagkapu.
	 */
	public Boolean isStargateOnIt = false;
	
	/** 
	 * Beallitja az adott csillagkaput, a specialis falon lesz.
	 * @param stargate A csillagkapu.
	 */
	public void setStargate(Stargate stargate) {
		System.out.printf("SpecialWall.setStargate() (color=%s)\n", stargate.color);
		this.stargate = stargate;
		if(stargate==null) isStargateOnIt = false;
		else isStargateOnIt = true;
	}
	
	/** 
	 * Van-e csillagkapu a specialis falon.
	 * @return Van/Nincs.
	 */
	public Boolean isStargate() {
		System.out.printf("SpecialWall.isStargate() (isStargateOnIt=%b)\n", isStargateOnIt); 
		return isStargateOnIt;
	}
	
	/** 
	 * A fregjarat tul oldalan levo csillagkapu elotti mezot adja vissza, ha van rajta csillagkapu es van feregjarat.
	 * @param direction Az irany. 
	 * @return A mezo.
	 */
	@Override
	public Tile getTile(Direction direction) {
		System.out.println("SpecialWall.getTile()");
		//Ha van rajta csillagkapu, akkor attol keri el (feregjatat tul oldalan levo kell ilyenkor)
		if(stargate != null) return stargate.getTile(direction);
		else return null;
	}
	
	/**
	 * A specialis fal elotti mezot adja vissza. (A sima oldal (Side) getTile() metodusara vezetjuk vissza.
	 * @param direction Az irany.
	 * @return A mezo.
	 */
	public Tile getRealTile(Direction direction) {
		return super.getTile(direction);
	}
	
	/**
	 * Atjarhato-e a Specialis Fal.
	 * @return Igen/Nem.
	 */
	@Override
	public Boolean canMove() {
		System.out.printf("SpecialWall.canMove()");
		if(stargate == null){
			System.out.printf(" (isStargateOnIt=%b)\n", isStargateOnIt);
			return false;
		}else{
			System.out.printf("\n");
			return stargate.canMove();
		}
	}
	
	/**
	 * Nyithato-e rajta csillagkapu.
	 * @return Igen.
	 */
	@Override
	public Boolean isStargatePlacable() {
		System.out.printf("SpecialWall.isStargatePlacable() (isPlaceable=%b)\n", true); 
		return true;
	}
	
	/**
	 * Visszaadja a csillagkaput, ha van a specialis falon.
	 * @return A csillagkapu. null, ha nincs.
	 */
	public Stargate getStargate() {
		System.out.println("SpecialWall.getStargate()");
		return stargate;
	}
	
}
