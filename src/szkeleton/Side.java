package szkeleton;
import java.util.HashMap;


/**
 * Oldal.
 *
 */
public class Side {
	
	/**
	 * Az oldalhoz tartozo mezok.
	 */
	HashMap<Direction, Tile> tiles = new HashMap<Direction,Tile>();;
	/**
	 * Atjarhato-e.
	 */
	public Boolean permeable = true;

	/**
	 * Atlehet-e az oldalon menni.
	 * @return Igen/Nem.
	 */
	public Boolean canMove() {
		System.out.printf("Side.canMove() (permeable=%b)\n", permeable);
		return permeable;
	}

	/** 
	 * Atlehet-e loni rajta.
	 * @return Igen/Nem.
	 */
	public Boolean canShootThrough () {
		System.out.printf("Side.canShootThrough() (permeable=%b)\n", permeable);
	    return permeable;
	}

	/** 
	 * Adott iranyban levo mezot adja vissza.
	 * @param direction Az irany.
	 * @return A mezo.
	 */
	public Tile getTile(Direction direction) {
		System.out.println("Side.getTile()");
		return tiles.get(direction);
	}

	/**
	 * Teheto-e ra csillagkapu.
	 * @return Igen/Nem.
	 */
	public Boolean isStargatePlacable() {
		System.out.printf("Side.isStargatePlacable() (isPlaceable=%b)\n", false);
	    return false;
	}
	
}
