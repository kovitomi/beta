package szkeleton;


/** 
 * Palya.
 *
 */
public class Map {

	/**
	 * Az ezredes.
	 */
	public Colonel oniel = null;
	/**
	 * A feregjarat.
	 */
	public Portal portal = null;
	
	/**
	 * Palya konstruktor.
	 * @param newOniel Az ezredes.
	 * @param newPortal A feregjarat.
	 */
	public Map(Colonel newOniel, Portal newPortal) {
		oniel = newOniel;
		portal = newPortal;
	}
	
	/**
	 * Megkeresi, hogy mibe utkozik az ezredes lovese, es szol a feregjaratnak.
	 * @param tile A kiindulo mezo.
	 * @param direction Az irany, amerre lo.
	 * @param color A lovedek szine.
	 */
	public void shoot(Tile tile, Direction direction, Color color) {
		System.out.println("Map.shoot()");
		Side tileSide = tile.tileSide(direction);
		
		//Megkeresi egyenes volnalban a legkozelebbi falat
		while(tileSide.canShootThrough()) {
			tileSide = tileSide.getTile(direction).tileSide(direction);
		}
		Stargate created = portal.stargateShot(tileSide, color);
		if(created!=null) {
			created.setDirection(direction.getOpposite());
		}
	}
	
}
