package szkeleton;
import java.util.HashMap;


/** 
 * Feregjarat.
 *
 */
public class Portal {
	
	/**
	 * Az ezredes.
	 */
	public Colonel oniel = null;
	/**
	 * Nyitva van-e a feregjarat.
	 */
	public Boolean isOpen = false;
	/**
	 * A feregjarathoz tartozo csillagkapuk.
	 */
	public HashMap<Color, Stargate> stargates = new HashMap<Color, Stargate>();
	/**
	 * A feregjarathoz tartozo specialis falak (amiken a csillagkapuk vannak).
	 */
	public HashMap<Color, SpecialWall> specialWalls = new HashMap<Color, SpecialWall>();

	/**
	 * Feregjarat konstruktor.
	 */
	public Portal() {
		oniel = null;
		isOpen = false;
		stargates = new HashMap<Color, Stargate>();
		specialWalls = new HashMap<Color, SpecialWall>();
	}
	
	/**
	 * Feregjarat átjarhatosaganak ellenorzese.
	 */
	private void setPermeable() {
		System.out.println("Portal.setPermeable()");
		int n=0;
		if(stargates.get(Color.Orange)!=null) n++;
		if(stargates.get(Color.Blue)!=null) n++;
		if(n==2) isOpen = true;
		else isOpen = false;
	}
	/**
	 * Az ezredes loveset kezelei a becsapodas pillanataban. 
	 * @param side Ahova a csillagkapunak kerulnie kell. 
	 * @param color A csillagkapu szine.
	 * @return A letrejott csillagkapu, null ha nem tudott letrehozni.
	 */
	public Stargate stargateShot(Side side, Color color) {
		System.out.println("Portal.stargateShot()");
		if(side.isStargatePlacable()) {
			SpecialWall theSideWall = (SpecialWall)side;
			if(theSideWall.isStargate()) {
				Stargate oldStargate = theSideWall.getStargate();
				if(!oldStargate.getColor().equals(color)) {
					Stargate newStargate = new Stargate(color, this, oniel);
					
					//Toroljuk az elozoleg letezo megegyezo szinu csilagkaput a specialis falrol
					if(specialWalls.get(color) != null){
						specialWalls.get(color).setStargate(null);
						specialWalls.remove(color.getOther());
					}
					
					//Toroljuk a masik szinu csilagkaput (mert felul kell irnunk, ugyan oda lottuk)
					stargates.remove(color.getOther());
					
					//Beallitjuk az ujat a specialis falon, ezzel felulirjuk a megegyezo szinu csillagkaput
					specialWalls.put(color, theSideWall);
					theSideWall.setStargate(newStargate);
					
					//Feregjaraton is beallitjuk
					stargates.put(color, newStargate);
					setPermeable();
					return newStargate;
				} else {
					return null;
				}
			} else {
				Stargate newStargate = new Stargate(color, this, oniel);
				//Toroljuk a mar letezo ilyen szinut
				if(specialWalls.get(color) != null){
					specialWalls.get(color).setStargate(null);
					specialWalls.remove(color);
				}
				//Beallitjuk az ujat
				stargates.put(color, newStargate);
				specialWalls.put(color, theSideWall);
				theSideWall.setStargate(newStargate);
				setPermeable();
				return newStargate;
			}
		}
	    return null;
	}

	/**
	 * Atjarhato-e a feregjarat.
	 * @return Igen/Nem.
	 */
	public Boolean isPermeable() {
		System.out.printf("Portal.isPermeable() (isOpen=%b)\n", isOpen);
		return isOpen;
	}

	/**
	 * Azt az iranyt adja vissza, amerre az adott szinu csillagkapu nez.
	 * @param color A szin.
	 * @return Az Irany.
	 */
	public Direction getDirection(Color color) {
		System.out.println("Portal.getDirection()");
		return stargates.get(color).getDirection();
	}

	/**
	 * Az adott szinu csillagkapu elotti mezot adja vissza.
	 * @param color A szin.
	 * @return A mezo.
	 */
	public Tile getTile(Color color) {
		System.out.printf("Portal.getTile() (color=%s)\n", color);
		return specialWalls.get(color).getRealTile(stargates.get(color).getDirection());
	}
	
}
