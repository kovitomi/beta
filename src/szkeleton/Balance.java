package szkeleton;

/** 
 * Merleg.
 *
 */
public class Balance {
	
	/**
	 * Van-e rajta suly.
	 */
	public Boolean haveWeightOnIt = false;
	/**
	 * A merleghez tartozo ajto.
	 */
	public Door door = null;
	
	/**
	 * Merleg konstruktor.
	 * @param newDoor Ajtot beallitjuk.
	 * @param newHaveWeightOnIt Van-e rajta suly.
	 */
	public Balance(Door newDoor, Boolean newHaveWeightOnIt) {
		door = newDoor;
		haveWeightOnIt = newHaveWeightOnIt;
	}
	/**
	 * Sulyt helyezunk ra.
	 */
	public void placeWeightOnIt() {
		System.out.println("Balance.placeWeightOnIt()");
		haveWeightOnIt = true;
		door.setOpen(true);
	}
	
	/**
	 * Sulyt veszunk el rola.
	 */
	public void takeWeightFromIt() {
		System.out.println("Balance.takeWeightFromIt()");
		haveWeightOnIt = false;
		door.setOpen(false);
	}
	
	/**
	 * Visszaadja az ajtot, amit mukodtet.
	 * @return Az ajto.
	 */
	public Door getDoor() {
		System.out.println("Balance.getDoor()");
		return door;
	}
	
}
