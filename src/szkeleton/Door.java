package szkeleton;

/** 
 * Ajto.
 *
 */
public class Door extends Side {

	/**
	 * Az oldalhoz rendelt merleg.
	 */
	public Balance balance = null;
	
	/**
	 * Ajto konstruktor.
	 * @param newBalance A merleg, ami kezeli.
	 */
	public Door(Balance newBalance) {
		balance = newBalance;
		permeable = false;
	}
	
	/**
	 * Parametertol fuggoen nyilik, zarodik.
	 * @param open Nyitva/Zarva.
	 */
	public void setOpen(Boolean open) {
		System.out.println("Door.setOpen");
		permeable = open;
	}
	
}
