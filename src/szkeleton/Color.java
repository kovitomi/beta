package szkeleton;

/**
 * Szinek.
 *
 */
public enum Color {
	Blue, Orange;
	
	/**
	 * Visszaadja a masik szint.
	 * @return A szin.
	 */
	public Color getOther() {
		if(this == Blue) return Orange;
		else return Blue;
	}
	
}
