package szkeleton;

/**
 * Iranyok.
 *
 */
public enum Direction {
	North, South, East, West; 
	
	/**
	 * Visszaadja az ellentetes iranyt.
	 * @return
	 */
	public Direction getOpposite(){
		//North -> South
		if(this == Direction.North)
			return Direction.South;
		//East -> West
		else if(this == Direction.East)
			return Direction.West;
		//South -> North
		else if(this == Direction.South)
			return Direction.North;
		//West -> East
		else
			return Direction.East;
	}
	
}
