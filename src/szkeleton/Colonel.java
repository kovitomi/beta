package szkeleton;

/** 
 * Ezredes.
 *
 */
public class Colonel {

	/**
	 * A mezo, amin all.
	 */
	public Tile location = null;
	/**
	 * Az irany, amerre nez.
	 */
	public Direction viewDirection = Direction.North;
	/**
	 * Van-e nala doboz.
	 */
	public Boolean box = false;
	/**
	 * Osszegyujtott zpm-ek szama.
	 */
	public int zpm = 0;
	/**
	 * Osszegyujtendo zpm-ek szama.
	 */
	public int maxzpm = 1;
	/**
	 * A jatek.
	 */
	Game game = null;
	/**
	 * A terkeps.
	 */
	Map map;
	
	/**
	 * Az ezredes megprobal elmozdulni egy mezonyit adott iranyban.
	 * @param direction Az irany.
	 */
	public void move(Direction direction) {
		System.out.println("Colonel.move()");
		look(direction);
		Side inWay = location.tileSide(direction);
		if(inWay.canMove()) {
			Tile newLocation = inWay.getTile(direction);
			location.leave(newLocation, direction, this);
		}
	}
	
	/**
	 * Az ezredes adott iranyba fordul el.
	 * @param direction Az irany.
	 */
	public void look(Direction direction) {
		System.out.printf("Colonel.look() (direction=%s)\n", direction);
		this.viewDirection = direction;
	}
	
	/** 
	 * Megprobal dobozt felvenni a kozvetlen elotte levo mezobol.
	 */
	public void pickUpBox() {
		System.out.printf("Colonel.pickUpBox() (haveBox=%b)\n", box);
		if(box) return;

		Side inWay = location.tileSide(viewDirection);
		if(inWay.canMove()) {
			Tile boxLocation = inWay.getTile(viewDirection);
			boxLocation.gatherBox(this);
		}
	}
	
	/**
	 * Megprobal dobozt letenni a kozvetlenul elotte allo mezobe.
	 */
	public void putDownBox() {
		System.out.printf("Colonel.putDownBox() (haveBox=%s)\n", box);
		if(!box) return;
		
		Side inWay = location.tileSide(viewDirection);
		if(inWay.canMove()) {
			Tile boxLocation = inWay.getTile(viewDirection);
			boxLocation.placeBox(this);
		}
	}
	
	/**
	 * ZPM modult vesz fel a mezorol, amire erkezett.
	 */
	public void pickUpZPM() {
		System.out.println("Colonel.pickUpZPM()");
		zpm++;
		if(zpm == maxzpm) game.victory();
	}
	
	/** 
	 * Az ezredes adott szinnel lo abba az iranyba, amerre nez.
	 * @param color A szin.
	 */
	public void shootPortal(Color color) {
		System.out.println("Colonel.shootPortal()");
		map.shoot(location, viewDirection, color);
	}
	
	/** 
	 * Beallitja az ezredes uj tartozkodasi helyet.
	 * @param newLocation
	 */
	public void setLocation(Tile newLocation) {
		System.out.println("Colonel.setLocation()");
		location = newLocation;
	}
	
	/** 
	 * Az ezredesnek adnak egy dobozt.
	 */
	public void giveBox() {
		System.out.println("Colonel.giveBox()");
		box = true;
	}
	
	/** 
	 * Az ezredestol elveszik dobozt.
	 */
	public void takeBox() {
		System.out.println("Colonel.takeBox()");
		box = false;
	}
	
	/**
	 * Meghal az ezredes.
	 */
	public void die() {
		System.out.println("Colonel.die()");
		game.lose();
	}
	
}
