package szkeleton;
import java.util.HashMap;


/** 
 * Mezo.
 *
 */
public class Tile{

	/** 
	 * A mezohoz tartozo oldalak.
	 */
	HashMap<Direction, Side> sides = new HashMap<Direction, Side>();
	/** 
	 * A mezohoz tartozo merleg.
	 */
	public Balance balance = null;
	/** 
	 * Van-e doboz a mezon.
	 */
	public Boolean box = false;
	/** 
	 * Van-e zpm a mezon.
	 */
	public Boolean zpm = false;

	/**
	 * Adott iranyban visszaadja a hatarolo oldalt.
	 * @param direction Az irany.
	 * @return Az odlal.
	 */
	public Side tileSide(Direction direction) {
		System.out.println("Tile.tileSide()");
		return sides.get(direction);
	}
	
	/**
	 * A mezon van-e doboz.
	 * @param is Igen/Nem.
	 */
	public void setBox(Boolean is) {
		System.out.println("Tile.setBox()");
		box = is;
		if(balance != null){
			if(is)balance.placeWeightOnIt();
			else balance.takeWeightFromIt();
		}
	}
	
	/**
	 * Az ezredes megerkezik a mezore.
	 * @param oniel Az ezredes.
	 */
	public void arrive(Colonel oniel) {
		System.out.println("Tile.arrive()");
		oniel.setLocation(this);
		if(balance != null){
			balance.placeWeightOnIt();
		}
		if(zpm) oniel.pickUpZPM();
	}
	
	/**
	 * Elhagyja a mezot az ezredes, amin meghivjuk.
	 * @param newLocation A mezo, amire lep.
	 * @param direction Az irany, amerre megy.
	 * @param oniel Az ezredes.
	 */
	public void leave(Tile newLocation, Direction direction, Colonel oniel) {
		System.out.println("Tile.leave()");
		if(box || balance == null) newLocation.arrive(oniel);
		else {
			if(balance != null ) {
				//Ha az ajto, ami a mezoben megegyezik azzal, amit a mezoben levo merleg nyit, nem mehet at az ezredes (emrt razarodna)
				if(!balance.getDoor().equals(sides.get(direction))) {
					newLocation.arrive(oniel);
					balance.takeWeightFromIt();
				}
			} else {
				newLocation.arrive(oniel);
			}
		}
	}
	
	/**
	 * Az ezredes elveszi a mezon levo dobozt, ha van.
	 * @param oniel Az ezredes.
	 */
	public void gatherBox(Colonel oniel) {
		System.out.printf("Tile.gatherBox() (boxInRoom=%b)\n", box);
		if(box){
			setBox(false);
			oniel.giveBox();
		}
	}
	
	/** 
	 * Az ezredes dobozt helyez el a mezon, ha azon nincs meg.
	 * @param oniel Az ezredes.
	 */
	public void placeBox(Colonel oniel) {
		System.out.printf("Tile.placeBox() (boxInRoom=%b)\n", box);
		if(!box) {
			oniel.takeBox();
			setBox(true);
		}
	}
	
}
