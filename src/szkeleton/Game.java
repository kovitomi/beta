package szkeleton;
import java.io.File;
import java.util.Scanner;


/** 
 * Jatek.
 *
 */
public class Game {
	
	/**
	 * A program fő metódusa.
	 * @param args
	 */
	public static void main(String[] args) {	
        int menuNumber = 0;
        int subMenuNumber = 0;
        Scanner input = new Scanner( System.in );
        
		while(true){
	        System.out.println("Valaszd ki a futtatni akart esetet:");
	        System.out.println("1-7: egyszeru teszt esetek:");
	        System.out.println("  8: összetett teszt esetek:");	        
	        System.out.println("1,1: Iranyvaltoztatas:");
	        System.out.println("2,1: Atjarhato-e az ures atjaro:");
	        System.out.println("2,2: Atjarhato-e a nyitott ajto:");
	        System.out.println("2,3: Atjarhato-e a zart ajto:");
	        System.out.println("2,4: Atjarhato-e a nyitott feregjarat:");
	        System.out.println("2,5: Atjarhato-e a zart feregjarat:");
	        System.out.println("2,6: Atjarhato-e a fal:");
	        System.out.println("2,7: Atjarhato-e a specialis fal:");
	        System.out.println("3,1: Atlepes mezore, ahol nincs zpm:");
	        System.out.println("3,2: Atlepes mezore, ahol van zpm:");
	        System.out.println("3,3: Atlepes szakadekba:");
	        System.out.println("4,1: Keresztul, illetve ra lehet-e loni falra:");
	        System.out.println("4,2: Keresztul, illetve ra lehet-e loni atjarora, majd falra:");
	        System.out.println("4,3: Keresztul, illetve ra lehet-e loni specialis falra:");
	        System.out.println("4,4: Uj csillagkapu nyitasa nem egyezo szinu csillagkapun:");
	        System.out.println("4,5: Uj csillagkapu nyitasa egyezo szinu csillagkapun:");
	        System.out.println("4,6: Uj csillagkapu nyitasa ures specialis falon:");
	        System.out.println("4,7: Feregjarat megnyitasa:");
	        System.out.println("5,1: Nem tud felvenni dobozt abbol az iranybol:");
	        System.out.println("5,2: Nem tud felvenni dobozt, mert nincs abba az iranba:");
	        System.out.println("5,3: Nem tud felvenni dobozt, mert mar van nala:");
	        System.out.println("5,4: Fel tud venni dobozt:");
	        System.out.println("6,1: Nem tud letenni dobozt abba az iranyba:");
	        System.out.println("6,2: Nem tud letenni dobozt, mert nincs nala:");
	        System.out.println("6,3: Nem tud letenni dobozt, mert mar van abba a mezobe:");
	        System.out.println("6,4: Le tud tenni dobozt a mezobe:");
	        System.out.println("6,5: Le tud tenni dobozt a szakadekba, de az eltunik:");
	        System.out.println("7,1: Rakerul a suly a merlegre:");
	        System.out.println("7,2: Lekerul a suly a merlegrol:");
	        System.out.println("7,3: (spec) Nem tud atlepni olyan mezore, amitol eppen az az ajto valasztja el, aminek a merlegen all:");
	        System.out.println("8,1: Ezredes mozgatasa eszakra egy szakadekba:");
	        System.out.println("8,2: Ezredes mozgatasa feregjaraton keresztul egy szakadekba:");
	        System.out.println("8,3: Suly felvetele, merlegre rakasa, majd atmozgas a nyitott ajton egy szakadekba:");
	        System.out.println("8,4: Ezredessel loves egy atjaron keresztul egy ures specialis falra:");
	        System.out.println("8,5: Ezredessel loves egy atjaron keresztul egy ellentetes szinu csillagkapura:");
	        System.out.println("8,6: Ezredessel loves, majd atmozgas a nyitott feregjaraton:");
	        System.out.println("9: Kilepes:");
	        System.out.println("");
	        System.out.print( "Fomenu szama: " );
	        menuNumber = input.nextInt();
	        if(menuNumber == 9)
	        	break;
	        
	        System.out.print( "Almenu szama: " );
	        subMenuNumber = input.nextInt();
	        System.out.println();
	        
			Game game = new Game();
			//game.initialize(null);
			Colonel oniel = new Colonel();
			Portal portal = new Portal();
			Map map = new Map(oniel, portal);
			oniel.game = game;
			oniel.map = map;
			portal.oniel = oniel;
			
			oniel.maxzpm = 1;
			
			if(menuNumber == 1 && subMenuNumber == 1){
				System.out.printf("%d,%d: Iranyvaltoztatas:\n",menuNumber,subMenuNumber);
				oniel.look(Direction.West);
			}
			
			if(menuNumber == 2 && subMenuNumber == 1){
				System.out.printf("%d,%d: Atjarhato-e az ures atjaro:\n",menuNumber,subMenuNumber);
				Side s1 = new Side();
				
				s1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 2){
				System.out.printf("%d,%d: Atjarhato-e a nyitott ajto:\n",menuNumber,subMenuNumber);
				Door d1 = new Door(null);
				
				d1.permeable = true;
				
				d1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 3){
				System.out.printf("%d,%d: Atjarhato-e a zart ajto:\n",menuNumber,subMenuNumber);
				Door d1 = new Door(null);
				
				d1.permeable = false;
				
				d1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 4){
				System.out.printf("%d,%d: Atjarhato-e a nyitott feregjarat:\n",menuNumber,subMenuNumber);
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				Stargate sg2 = new Stargate(Color.Orange, portal, oniel);
				
				sg1.viewDirection = Direction.South;
				sg1.oniel = oniel;
				sg2.color = Color.Orange;
				sg2.otherColor = Color.Blue;
				sg2.viewDirection = Direction.East;
				sg2.oniel = oniel;
				portal.isOpen = true;
				portal.stargates.put(Color.Blue, sg1);
				portal.stargates.put(Color.Orange, sg2);
				
				sg1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 5){
				System.out.printf("%d,%d: Atjarhato-e a zart feregjarat:\n",menuNumber,subMenuNumber);
				Stargate sg1 = new Stargate(Color.Orange, portal, oniel);
				
				portal.isOpen = false;
				
				sg1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 6){
				System.out.printf("%d,%d: Atjarhato-e a fal:\n",menuNumber,subMenuNumber);
				Wall w1 = new Wall();
				
				w1.canMove();
			}
			
			if(menuNumber == 2 && subMenuNumber == 7){
				System.out.printf("%d,%d: Atjarhato-e a specialis fal:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				
				sw1.canMove();
			}
	
			if(menuNumber == 3 && subMenuNumber == 1){
				System.out.printf("%d,%d: Atlepes mezore, ahol nincs zpm:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				t1.sides.put(Direction.North, s1);
				oniel.location = t1;
				
				t1.leave(t2, Direction.North, oniel);
			}	
			
			if(menuNumber == 3 && subMenuNumber == 2){
				System.out.printf("%d,%d: Atlepes mezore, ahol van zpm:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();

				t2.zpm = true;
				t1.sides.put(Direction.North, s1);			
				oniel.location = t1;
				
				t1.leave(t2, Direction.North, oniel);
			}
			
			if(menuNumber == 3 && subMenuNumber == 3){
				System.out.printf("%d,%d: Atlepes szakadekba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Abyss a1 = new Abyss();
				Side s1 = new Side();
	
				t1.sides.put(Direction.North, s1);
				oniel.location = t1;
				
				t1.leave(a1, Direction.North, oniel);
			}
			
			if(menuNumber == 4 && subMenuNumber == 1){
				System.out.printf("%d,%d: Keresztul, illetve ra lehet-e loni falra:\n",menuNumber,subMenuNumber);
				Wall w1 = new Wall();
				
				w1.canShootThrough();
				w1.isStargatePlacable();
			}
			
			if(menuNumber == 4 && subMenuNumber == 2){
				System.out.printf("%d,%d: Keresztul, illetve ra lehet-e loni atjarora, majd falra:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				Wall w1 = new Wall();
				
				t1.sides.put(Direction.North, s1);
				t2.sides.put(Direction.North, w1);
				t2.sides.put(Direction.South, s1);
				
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);	
				w1.tiles.put(Direction.South, t1);
				
				Tile newLocation = t1;
				Side newSide = s1;
				newSide.isStargatePlacable();
				
				while(newSide.canShootThrough()){
					newLocation = newSide.getTile(oniel.viewDirection);
					newSide = newLocation.tileSide(oniel.viewDirection);
					newSide.isStargatePlacable();
				}
			}
			
			if(menuNumber == 4 && subMenuNumber == 3){
				System.out.printf("%d,%d: Keresztul, illetve ra lehet-e loni specialis falra:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				
				sw1.canShootThrough();
				sw1.isStargatePlacable();
			}
			
			if(menuNumber == 4 && subMenuNumber == 4){
				System.out.printf("%d,%d: Uj csillagkapu nyitasa nem egyezo szinu csillagkapun:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				sw1.isStargateOnIt = true;
				Color newColor = Color.Orange;
				
				if(sw1.isStargate())
				{
					Color oldColor = sg1.getColor();
					if(newColor != oldColor){
						sg1 = null;
						sg1 = new Stargate(newColor, portal, oniel);
						sw1.setStargate(sg1);
					}
				}
			}
			
			if(menuNumber == 4 && subMenuNumber == 5){
				System.out.printf("%d,%d: Uj csillagkapu nyitasa egyezo szinu csillagkapun:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				sw1.isStargateOnIt = true;
				Color newColor = Color.Blue;
				
				if(sw1.isStargate())
				{
					Color oldColor = sg1.getColor();
					if(newColor != oldColor){
					}
				}
			}
			
			if(menuNumber == 4 && subMenuNumber == 6){
				System.out.printf("%d,%d: Uj csillagkapu nyitasa ures specialis falon:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				SpecialWall sw2 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				sw1.isStargateOnIt = true;
				Color newColor = Color.Blue;
				
				if(sw2.isStargate())
				{
				}else{
					sg1 = null;
					sg1 = new Stargate(newColor, portal, oniel);
					sw2.setStargate(sg1);
				}
			}
			
			if(menuNumber == 4 && subMenuNumber == 7){
				System.out.printf("%d,%d: Feregjarat megnyitasa:\n",menuNumber,subMenuNumber);
				SpecialWall sw1 = new SpecialWall();
				SpecialWall sw2 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Orange, portal, oniel);
				Stargate sg2 = new Stargate(Color.Blue, portal, oniel);
				
				portal.specialWalls.put(Color.Orange, sw1);
				portal.specialWalls.put(Color.Blue, sw2);
				sw1.setStargate(sg1);
				sw2.setStargate(sg2);
				portal.stargates.put(Color.Orange, sg1);
				portal.stargates.put(Color.Blue, sg2);
				
				System.out.println("Portal.setPermeable()");
				int n=0;
				if(portal.stargates.get(Color.Orange)!=null) n++;
				if(portal.stargates.get(Color.Blue)!=null) n++;
				if(n==2) portal.isOpen = true;
				else portal.isOpen = false;
				
				sw1.canMove();
			}
			
			if(menuNumber == 5 && subMenuNumber == 1){
				System.out.printf("%d,%d: Nem tud felvenni dobozt abbol az iranybol:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Wall w1 = new Wall();
				
				t1.sides.put(Direction.North, w1);
				w1.tiles.put(Direction.South, t1);	
				oniel.location = t1;
				
				oniel.pickUpBox();
			}
			
			if(menuNumber == 5 && subMenuNumber == 2){
				System.out.printf("%d,%d: Nem tud felvenni dobozt, mert nincs abba az iranba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);	
				oniel.location = t1;
				
				oniel.pickUpBox();
			}
			
			if(menuNumber == 5 && subMenuNumber == 3){
				System.out.printf("%d,%d: Nem tud felvenni dobozt, mert mar van nala:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				t2.box = true;
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);	
				oniel.location = t1;
				oniel.box = true;
				
				oniel.pickUpBox();
			}
			
			if(menuNumber == 5 && subMenuNumber == 4){
				System.out.printf("%d,%d: Fel tud venni dobozt:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				t2.box = true;
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);	
				oniel.location = t1;
				
				oniel.pickUpBox();
			}
			
			if(menuNumber == 6 && subMenuNumber == 1){
				System.out.printf("%d,%d: Nem tud letenni dobozt abba az iranyba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Wall w1 = new Wall();
				
				t1.sides.put(Direction.North, w1);
				w1.tiles.put(Direction.South, t1);	
				oniel.box = true;
				oniel.location = t1;
				
				oniel.putDownBox();
			}
			
			if(menuNumber == 6 && subMenuNumber == 2){
				System.out.printf("%d,%d: Nem tud letenni dobozt, mert nincs nala:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Wall w1 = new Wall();
				
				t1.sides.put(Direction.North, w1);
				w1.tiles.put(Direction.South, t1);	
				oniel.location = t1;
				
				oniel.putDownBox();
			}
			
			if(menuNumber == 6 && subMenuNumber == 3){
				System.out.printf("%d,%d: Nem tud letenni dobozt, mert mar van abba a mezobe:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				oniel.box = true;
				t2.box = true;
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);	
				s1.tiles.put(Direction.North, t2);	
				oniel.location = t1;
				
				oniel.putDownBox();
			}
			
			if(menuNumber == 6 && subMenuNumber == 4){
				System.out.printf("%d,%d: Le tud tenni dobozt a mezobe:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				
				oniel.box = true;
				t1.sides.put(Direction.North, s1);	
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);
				oniel.location = t1;
				
				oniel.putDownBox();
			}
			
			if(menuNumber == 6 && subMenuNumber == 5){
				System.out.printf("%d,%d: Le tud tenni dobozt a szakadekba, de az eltunik:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Abyss a1 = new Abyss();
				Side s1 = new Side();
				
				oniel.box = true;
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, a1);
				oniel.location = t1;
				
				oniel.putDownBox();
			}
			
			if(menuNumber == 7 && subMenuNumber == 1){
				System.out.printf("%d,%d: Rakerul a suly a merlegre:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				t1.balance = new Balance(null, false);
				Door d1 = new Door(t1.balance);
				
				t1.balance.door = d1;
				
				d1.canMove();
				t1.balance.placeWeightOnIt();
				d1.canMove();
			}
			
			if(menuNumber == 7 && subMenuNumber == 2){
				System.out.printf("%d,%d: Lekerul a suly a merlegrol:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				t1.balance = new Balance(null, false);
				Door d1 = new Door(t1.balance);
				
				d1.permeable = true;
				t1.balance.door = d1;
				t1.balance.haveWeightOnIt = true;
				
				d1.canMove();
				t1.balance.takeWeightFromIt();
				d1.canMove();
			}
			
			if(menuNumber == 7 && subMenuNumber == 3){
				System.out.printf("%d,%d: (spec) Nem tud atlepni olyan mezore, amitol eppen az az ajto valasztja el, aminek a merlegen all:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				t1.balance = new Balance(null, false);
				Door d1 = new Door(t1.balance);
				
				t1.balance.door = d1;
				t1.sides.put(Direction.North, d1);
				d1.tiles.put(Direction.South, t1);
				d1.tiles.put(Direction.North, t2);
				oniel.location = t1;
				
				t1.balance.placeWeightOnIt();
				oniel.move(Direction.North);
			}
			
			if(menuNumber == 8 && subMenuNumber == 1){
				System.out.printf("%d,%d: Ezredes mozgatasa eszakra egy szakadekba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Abyss t2 = new Abyss();
				Side s1 = new Side();
				
				t1.sides.put(Direction.North, s1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);
				oniel.location = t1;
				
				oniel.move(Direction.North);
			}
			
			if(menuNumber == 8 && subMenuNumber == 2){
				System.out.printf("%d,%d: Ezredes mozgatasa feregjaraton keresztul egy szakadekba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Abyss t2 = new Abyss();
				SpecialWall sw1 = new SpecialWall();
				SpecialWall sw2 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				Stargate sg2 = new Stargate(Color.Orange, portal, oniel);
				
				sw1.isStargateOnIt = true;
				sw2.isStargateOnIt = true;
				sw1.stargate = sg1;
				sw2.stargate = sg2;
				t1.sides.put(Direction.North, sw1);
				t2.sides.put(Direction.West, sw2);
				sw1.tiles.put(Direction.South, t1);
				sw2.tiles.put(Direction.East, t2);				
				sg1.viewDirection = Direction.South;
				sg2.viewDirection = Direction.East;
				portal.stargates.put(Color.Blue, sg1);
				portal.stargates.put(Color.Orange, sg2);
				portal.specialWalls.put(Color.Blue, sw1);
				portal.specialWalls.put(Color.Orange, sw2);	
				portal.isOpen = true;
				oniel.location = t1;
				
				oniel.move(Direction.North);
			}
			
			if(menuNumber == 8 && subMenuNumber == 3){
				System.out.printf("%d,%d: Suly felvetele, merlegre rakasa, majd atmozgas a nyitott ajton egy szakadekba:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				t2.balance = new Balance(null, false);
				Tile t3 = new Tile();
				Abyss a1 = new Abyss();
				Side s1 = new Side();
				Side s2 = new Side();
				Door d1 = new Door(t2.balance);
				
				
				t2.balance.door = d1;
				t3.box = true;
				t1.sides.put(Direction.East, s1);
				t1.sides.put(Direction.West, s2);
				t1.sides.put(Direction.North, d1);
				s1.tiles.put(Direction.West, t1);
				s1.tiles.put(Direction.East, t3);
				s2.tiles.put(Direction.East, t1);
				s2.tiles.put(Direction.West, t2);
				d1.tiles.put(Direction.South, t1);
				d1.tiles.put(Direction.North, a1);
				oniel.location = t1;
				
				oniel.look(Direction.East);
				oniel.pickUpBox();
				oniel.look(Direction.West);
				oniel.putDownBox();
				oniel.move(Direction.North);
			}
			
			if(menuNumber == 8 && subMenuNumber == 4){
				System.out.printf("%d,%d: Ezredessel loves egy atjaron keresztul egy ures specialis falra:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				SpecialWall sw1 = new SpecialWall();
				
				t1.sides.put(Direction.North, s1);
				t2.sides.put(Direction.South, s1);
				t2.sides.put(Direction.North, sw1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);
				oniel.location = t1;
				
				oniel.shootPortal(Color.Blue);
			}
			
			if(menuNumber == 8 && subMenuNumber == 5){
				System.out.printf("%d,%d: Ezredessel loves egy atjaron keresztul egy ellentetes szinu csillagkapura:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Tile t2 = new Tile();
				Side s1 = new Side();
				SpecialWall sw1 = new SpecialWall();
				Stargate sg1 = new Stargate(Color.Blue, portal, oniel);
				
				sw1.isStargateOnIt = true;
				sw1.stargate = sg1;
				t1.sides.put(Direction.North, s1);
				t2.sides.put(Direction.South, s1);
				t2.sides.put(Direction.North, sw1);
				s1.tiles.put(Direction.South, t1);
				s1.tiles.put(Direction.North, t2);
				portal.stargates.put(Color.Blue, sg1);
				oniel.location = t1;
				
				oniel.shootPortal(Color.Orange);
			}
			
			if(menuNumber == 8 && subMenuNumber == 6){
				System.out.printf("%d,%d: Ezredessel loves, majd atmozgas a nyitott feregjaraton:\n",menuNumber,subMenuNumber);
				Tile t1 = new Tile();
				Abyss a1 = new Abyss();
				SpecialWall sw1 = new SpecialWall();
				SpecialWall sw2 = new SpecialWall();
				Stargate sg2 = new Stargate(Color.Blue, portal, oniel);
				
				sw2.isStargateOnIt = true;
				sw2.stargate = sg2;
				sg2.viewDirection = Direction.South;
				t1.sides.put(Direction.East, sw1);
				a1.sides.put(Direction.North, sw2);
				sw2.tiles.put(Direction.South, a1);
				portal.stargates.put(Color.Blue, sg2);
				portal.specialWalls.put(Color.Blue, sw2);
				oniel.location = t1;
				
				oniel.look(Direction.East);
				oniel.shootPortal(Color.Orange);
				oniel.move(Direction.East);
			}	
			
			System.out.println("");
			System.out.println("Nyomj le egy billentyut a folytatashoz...");
			try {
				System.in.read();
			} catch (Exception e) {}

		}	
		input.close();
	}
	
	/**
	 * Palya elokeszites.
	 * @param map A terkep fajl.
	 */
	public void initialize(File map) {
		System.out.println("Game.initialize()");
	}
	
	/**
	 * Vesztes.
	 */
	public void lose() {
		System.out.println("Game.lose()");
	}
	
	/**
	 * Gyozelem
	 */
	public void victory() {
		System.out.println("Game.victory()");
	}
	
}
